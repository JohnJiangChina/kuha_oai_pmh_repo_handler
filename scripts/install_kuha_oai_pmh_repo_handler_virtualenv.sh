#!/bin/bash

HERE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ ! -f "${HERE}/runtime_env" ]; then
    cp "${HERE}/runtime_env.dist" "${HERE}/runtime_env"
fi

source "${HERE}/runtime_env"

python3 -m venv "${KUHA_VENV_PATH}"

source "${KUHA_VENV_PATH}/bin/activate"

pip3 install --upgrade pip==20.3.4

cd "${HERE}/.."

pip3 install -r requirements.txt --upgrade --upgrade-strategy=only-if-needed

pip3 install . --upgrade --upgrade-strategy=only-if-needed
