#!/bin/bash

HERE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source "${HERE}/runtime_env"

source "${KUHA_VENV_PATH}/bin/activate"

kuha_list_records "$@"
