# Author(s): Toni Sissala
# Copyright 2020 Finnish Social Science Data Archive FSD / University of Tampere
# Licensed under the EUPL. See LICENSE.txt for full license.
"""OAI-PMH Repo handler constants.
"""
TEMPLATE_FOLDER = 'templates'
SERVER_PORT = 6003
API_VERSION = 'v0'
