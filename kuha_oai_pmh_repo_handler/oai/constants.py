# Author(s): Toni Sissala
# Copyright 2020 Finnish Social Science Data Archive FSD / University of Tampere
# Licensed under the EUPL. See LICENSE.txt for full license.
"""OAI constants
"""
OAI_REC_NAMESPACE_IDENTIFIER = None
OAI_REC_IDENTIFIER_PREFIX = 'oai'
OAI_RESPONSE_LIST_SIZE = 100
OAI_PROTOCOL_VERSION = 2.0
OAI_RESPONSE_TIMESTAMP_FORMAT = '%Y-%m-%dT%H:%M:%SZ'
OAI_RESPONSE_DATE_FORMAT = '%Y-%m-%d'
OAI_REPO_NAME = 'Kuha2 oai-pmh repository'
OAI_DEL_RECORDS_DECL_NO = 'no'
OAI_RESPOND_WITH_REQ_URL = False
REGEX_LOCAL_IDENTIFIER = r"[a-zA-Z0-9\-_\.!~\*'\(\);/\?:@&=\+$,%]+"
#: Regex to validate oai-identifier.
#: http://www.openarchives.org/OAI/2.0/guidelines-oai-identifier.htm
REGEX_OAI_IDENTIFIER = r"oai:[a-zA-Z][a-zA-Z0-9\-]*(\.[a-zA-Z][a-zA-Z0-9\-]*)+:" + REGEX_LOCAL_IDENTIFIER
#: Sets not complying with this regular expression are invalid according to OAI-PMH schema:
#: see: http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd
REGEX_SETSPEC = r"([A-Za-z0-9\-_\.!~\*'\(\)])+(:[A-Za-z0-9\-_\.!~\*'\(\)]+)*"
