#!/usr/bin/env python3
# Author(s): Toni Sissala
# Copyright 2020 Finnish Social Science Data Archive FSD / University of Tampere
# Licensed under the EUPL. See LICENSE.txt for full license.
"""Errors for OAI-protocol
"""


class OAIError(Exception):
    """Base for OAI errors
    """
    code_ = None
    def __init__(self, msg=None, context=None):
        super().__init__(msg)
        if msg:
            self.msg = msg
        self.context = context

    def __str__(self):
        _str = "{}: {}".format(self.get_code(), self.get_msg())
        if self.context:
            _str += " [{}]".format(self.get_context())
        return _str

    def __repr__(self):
        return str(self.code_)

    def get_code(self):
        """Get OAI error code"""
        return self.code_

    def get_msg(self):
        """Get OAI error message"""
        return self.msg

    def get_context(self):
        """Get error context"""
        return self.context

    def get_contextual_message(self):
        """Get error message with possible context.

        :returns: message with context.
        :rtype: str
        """
        msg = self.get_msg()
        ctx = self.get_context()
        if ctx is not None:
            msg = "%s: %s" % (msg, ctx)
        return msg


class MissingVerb(OAIError):
    """OAIError for missing verb"""
    code_ = 'badVerb'
    msg = 'Missing verb'


class BadVerb(OAIError):
    """OAIError for bad verb"""
    code_ = 'badVerb'
    msg = 'Invalid verb'


class NoMetadataFormats(OAIError):
    """OAIError for no metadata formats"""
    code_ = 'noMetadataFormats'
    msg = 'No metadata formats available'


class IdDoesNotExist(OAIError):
    """OAIError for no such id"""
    code_ = 'idDoesNotExist'
    msg = 'Identifier maps to no known item'


class BadArgument(OAIError):
    """OAIError for bad argument"""
    code_ = 'badArgument'
    msg = 'Illegal or missing argument'


class CannotDisseminateFormat(OAIError):
    """OAIError for cannot disseminate format"""
    code_ = 'cannotDisseminateFormat'
    msg = 'Metadata format not supported by this repository'


class NoRecordsMatch(OAIError):
    """OAIError for no records match"""
    code_ = 'noRecordsMatch'
    msg = 'Could not find records matching harvesting criteria'


class BadResumptionToken(OAIError):
    """OAIError for bad resumption token"""
    code_ = 'badResumptionToken'
    msg = ('Unable to resume harvesting with incomplete lists due to possible '
           'changes in the repository. Harvester is advised to restart '
           'the list request sequence.')
