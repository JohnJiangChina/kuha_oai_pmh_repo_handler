#!/usr/bin/env python3
# Author(s): Toni Sissala
# Copyright 2020 Finnish Social Science Data Archive FSD / University of Tampere
# Licensed under the EUPL. See LICENSE.txt for full license.
"""Defines OAI-PMH protocol.

Provides classes for handling requests and responses supported by
the protocol. Builds records from :mod:`kuha_common.document_store.records`.
"""
