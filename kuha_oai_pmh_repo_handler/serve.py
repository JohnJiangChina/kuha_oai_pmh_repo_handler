#!/usr/bin/env python3
# Author(s): Toni Sissala
# Copyright 2020 Finnish Social Science Data Archive FSD / University of Tampere
# Licensed under the EUPL. See LICENSE.txt for full license.
"""Main entry point for starting OAI-PMH Repo Handler.
"""
import sys

import logging
import traceback

import kuha_common.server

from kuha_oai_pmh_repo_handler.configure import configure
from kuha_oai_pmh_repo_handler.handlers import OAIRouteHandler
from kuha_oai_pmh_repo_handler.genshi_loader import (
    add_template_folder,
    set_template_writer
)


def get_app(api_version, app_settings=None):
    """Setup routes and return initialized Tornado web application.

    :param api_version: HTTP Api version gets prepended to routes.
    :type api_version: str
    :param app_settings: Settings to store to application.
    :type app_settings: dict or None.
    :returns: Tornado web application.
    :rtype: :obj:`tornado.web.Application`
    """
    handlers = [
        (kuha_common.server.str_api_endpoint(api_version, r"oai"), OAIRouteHandler)
    ]
    web_app = kuha_common.server.WebApplication(handlers)
    if app_settings:
        web_app.settings = app_settings
    return web_app


def main():
    """Application main function.

    Parse commandline for settings. Setup and serve webapp.
    Exit on exceptions propagated at this level.

    :returns: exit code, 1 on error, 0 on success.
    :rtype: int
    """
    rval = 0
    settings = configure()
    if settings.print_configuration:
        print("Print active configuration and exit\n")
        for key, value in settings._get_kwargs():
            print("{} ... {}".format(key, value))
        return rval
    add_template_folder(settings.template_folder)
    set_template_writer(OAIRouteHandler.template_writer)
    app_settings = {'kuha_settings': settings}
    web_app = get_app(settings.oai_pmh_api_version, app_settings)
    try:
        kuha_common.server.serve(web_app, settings.port)
    except KeyboardInterrupt as exc:
        rval = 1
        logging.warning("Shutdown by CTRL + C")
        logging.warning(str(exc))
    except Exception as exc:
        rval = 1
        logging.error(str(exc))
        traceback.print_exc()
    finally:
        # Cleanup
        logging.info("Shutdown")
    return rval


if __name__ == '__main__':
    sys.exit(main())
