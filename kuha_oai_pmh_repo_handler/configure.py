#!/usr/bin/env python3
# Author(s): Toni Sissala
# Copyright 2020 Finnish Social Science Data Archive FSD / University of Tampere
# Licensed under the EUPL. See LICENSE.txt for full license.
"""Configure OAI-PMH Repo Handler
"""

import os
from kuha_common import cli_setup
from kuha_oai_pmh_repo_handler.oai.protocol import (
    ResumptionToken,
    OAIResponse
)
from kuha_oai_pmh_repo_handler.oai.records import OAIHeaders
from kuha_oai_pmh_repo_handler.oai.constants import (
    OAI_REC_NAMESPACE_IDENTIFIER,
    OAI_RESPONSE_LIST_SIZE,
    OAI_PROTOCOL_VERSION,
    OAI_REPO_NAME,
    OAI_RESPOND_WITH_REQ_URL
)
from kuha_oai_pmh_repo_handler.constants import (
    TEMPLATE_FOLDER,
    SERVER_PORT,
    API_VERSION,
)


def configure(settings=None):
    """Get settings and configure application.

    Declares application spesific configuration options
    and some common options declared in :mod:`kuha_common.cli_setup`

    Configure application with arguments specified in
    configuration file, environment variables and command
    line arguments.

    :note: Calling this function multiple times
           will not initiate new settings to be parsed, but
           will return previously parsed settings instead.

    :param settings: Optional settings to use for configuration.
                     If settings are already loaded this parameter
                     will be silently ignored.
    :returns: settings
    :rtype: :obj:`argparse.Namespace`
    """
    if cli_setup.settings.is_settings_loaded():
        return cli_setup.get_settings()
    if not settings:
        cli_setup.load(os.path.dirname(os.path.realpath(__file__)),
                       description='Run Kuha OAI-PMH Repo Handler.',
                       config_file='kuha_oai_pmh_repo_handler.ini')
        cli_setup.add('--port',
                      help='Server port',
                      default=SERVER_PORT,
                      env_var='KUHA_OPRH_PORT',
                      type=int)
        cli_setup.add('--template-folder',
                      help='Folder containing XML templates',
                      default=cli_setup.prepend_abs_dir_path(TEMPLATE_FOLDER),
                      env_var='KUHA_OPRH_TEMPLATES',
                      type=str)
        cli_setup.add('--oai-pmh-repo-name',
                      help='OAI-PMH repository name',
                      default=OAI_REPO_NAME,
                      env_var='KUHA_OPRH_OP_REPO_NAME',
                      type=str)
        cli_setup.add('--oai-pmh-base-url',
                      help='OAI-PMH base url',
                      required=True,
                      env_var='KUHA_OPRH_OP_BASE_URL',
                      type=str)
        cli_setup.add('--oai-pmh-respond-with-requested-url',
                      help='OAI-PMH response uses base url of the request, rather than the configured base url.',
                      default=OAI_RESPOND_WITH_REQ_URL,
                      env_var='KUHA_OPRH_OP_RESP_WITH_REQ_URL',
                      action='store_true')
        cli_setup.add('--oai-pmh-namespace-identifier',
                      help='Namespace identifier to use with OAI-Identifiers.',
                      default=OAI_REC_NAMESPACE_IDENTIFIER,
                      env_var='KUHA_OPRH_OP_NAMESPACE_ID',
                      type=str)
        cli_setup.add('--oai-pmh-protocol-version',
                      help='OAI-PMH protocol version',
                      default=OAI_PROTOCOL_VERSION,
                      env_var='KUHA_OPRH_OP_PROTO_VERSION',
                      type=float)
        cli_setup.add('--oai-pmh-results-per-list',
                      help='How many results should a list response contain',
                      default=OAI_RESPONSE_LIST_SIZE,
                      env_var='KUHA_OPRH_OP_LIST_SIZE',
                      type=int)
        cli_setup.add('--oai-pmh-admin-email',
                      help='OAI-PMH administrator email address. Can be repeated for multiple addresses.',
                      action='append',
                      env_var='KUHA_OPRH_OP_EMAIL_ADMIN',
                      required=True,
                      type=str)
        cli_setup.add('--oai-pmh-api-version',
                      help='OAI-PMH api version',
                      default=API_VERSION,
                      env_var='KUHA_OPRH_API_VERSION',
                      type=str)
        cli_setup.add_print_configuration(cli_setup.settings.parser)
        settings = cli_setup.setup(
            cli_setup.MOD_DS_CLIENT,
            cli_setup.MOD_DS_QUERY,
            cli_setup.MOD_LOGGING
        )
    ResumptionToken.set_response_list_size(settings.oai_pmh_results_per_list)
    OAIHeaders.set_namespace_identifier(settings.oai_pmh_namespace_identifier)
    OAIResponse.set_protocol_version(settings.oai_pmh_protocol_version)
    OAIResponse.set_base_url(settings.oai_pmh_base_url)
    OAIResponse.set_admin_email(settings.oai_pmh_admin_email)
    OAIResponse.set_repository_name(settings.oai_pmh_repo_name)
    return settings
