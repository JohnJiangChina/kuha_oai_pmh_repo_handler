Installing Kuha OAI-PMH Repo Handler
------------------------------------

The operating system used in these steps is Ubuntu 16.04. Other modern
Linux variants may be used.

1. Create directory for OAI-PMH Repo Handler and Python virtualenv.

.. code-block:: bash

   mkdir kuha2

2. Clone package to subdirectory.

.. code-block:: bash

   git clone --single-branch --branch releases https://bitbucket.org/tietoarkisto/kuha_oai_pmh_repo_handler kuha2/kuha_oai_pmh_repo_handler

3. Install Python virtual environment.

.. code-block:: bash

   sudo apt install -y python3-venv

4. Make install script executable.

.. code-block:: bash

   chmod +x ./kuha2/kuha_oai_pmh_repo_handler/scripts/install_kuha_oai_pmh_repo_handler_virtualenv.sh

5. Install Kuha OAI-PMH Repo Handler to virtual environment.

.. code-block:: bash

   ./kuha2/kuha_oai_pmh_repo_handler/scripts/install_kuha_oai_pmh_repo_handler_virtualenv.sh

To run Kuha OAI-PMH Repo Handler you need access to Kuha Document Store.
First make the run script executable.

.. code-block:: bash

   chmod +x ./kuha2/kuha_oai_pmh_repo_handler/scripts/run_kuha_oai_pmh_repo_handler.sh

Run by calling the script. **Replace <document-store-url> with the URL to the Document Store**.
You also need to specify few configuration values for OAI-PMH: `base_url` and `admin_email`.

.. code-block:: bash

   ./kuha2/kuha_oai_pmh_repo_handler/scripts/run_kuha_oai_pmh_repo_handler.sh --document-store-url=<document-store-url> --oai-pmh-base-url=<base_url> --oai-pmh-admin-email=<email>

