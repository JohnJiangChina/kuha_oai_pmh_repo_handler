from unittest import mock
from inspect import iscoroutinefunction
from xml.etree import ElementTree as ET
import datetime
from tornado import testing
from kuha_common.testing import (
    MockCoro,
    testcases
)
from kuha_common.document_store import (
    Study,
    Variable,
    Question
)
from kuha_common.document_store.mappings.xmlbase import element_strip_descendant_text
from kuha_oai_pmh_repo_handler import (
    serve,
    handlers,
    genshi_loader,
    configure
)
from kuha_oai_pmh_repo_handler.oai.protocol import (
    as_supported_datestring,
    encode_uri,
    datetime as protocol_datetime
)
API_VERSION = 'v0'
OAI_URL = '/' + API_VERSION + '/oai'
XML_NS = {'oai': 'http://www.openarchives.org/OAI/2.0/'}
MD_PREFIXES = ('oai_dc', 'ddi_c', 'oai_ddi25', 'ead3')


def _query_single(result):
    async def _inner_query_single(record, on_record, **_discard):
        await on_record(result)
    return _inner_query_single


def _query_multiple(result):
    async def _inner_query_multiple(record, on_record, **_discard):
        is_coro = iscoroutinefunction(on_record)
        for rec in result[record.get_collection()]:
            if is_coro:
                await on_record(rec)
            else:
                on_record(rec)
    return _inner_query_multiple


def set_template_writer():
    if genshi_loader.WRITER == []:
        genshi_loader.set_template_writer(handlers.OAIRouteHandler.template_writer)


class HandlerTestBase(testing.AsyncHTTPTestCase):

    maxDiff = None

    settings_mock = mock.NonCallableMock(
        oai_pmh_results_per_list=configure.OAI_RESPONSE_LIST_SIZE,
        oai_pmh_namespace_identifier=configure.OAI_REC_NAMESPACE_IDENTIFIER,
        oai_pmh_base_url='base',
        oai_pmh_admin_email='email'
    )

    @classmethod
    def setUpClass(cls):
        set_template_writer()
        super().setUpClass()

    def setUp(self):
        self._requested_url = None
        self._patchers = []
        # Mock out query oontroller methods in order to control returned records
        self._mock_query_single = self._init_patcher(mock.patch.object(
            handlers.kuha_common.QueryController, 'query_single'))
        self._mock_query_multiple = self._init_patcher(mock.patch.object(
            handlers.kuha_common.QueryController, 'query_multiple'))
        self._mock_query_distinct = self._init_patcher(mock.patch.object(
            handlers.kuha_common.QueryController, 'query_distinct'))
        self._mock_query_count = self._init_patcher(mock.patch.object(
            handlers.kuha_common.QueryController, 'query_count'))
        super().setUp()

    def tearDown(self):
        for patcher in self._patchers:
            patcher.stop()

    def _init_patcher(self, patcher):
        mocked = patcher.start()
        self._patchers.append(patcher)
        return mocked

    def oai_request(self, return_record=None, return_relatives=None, **req_args):
        return_relatives = return_relatives or {}
        verb = req_args.get('verb', 'GetRecord')
        md_prefix = req_args.get('metadata_prefix', 'oai_dc')
        identifier = req_args.get('identifier', 'some_id')
        self._mock_query_single.side_effect = MockCoro(func=_query_single(return_record))
        self._mock_query_multiple.side_effect = MockCoro(func=_query_multiple(return_relatives))
        self._requested_url = (OAI_URL + '?verb={verb}&metadataPrefix={md_prefix}&'
                               'identifier={id}'.format(
                                   verb=verb, md_prefix=md_prefix, id=identifier))
        self._http_response = self.fetch(self._requested_url)
        return self._http_response

    def oai_list_request(self, verb='ListRecords', md_prefix='oai_dc', **optional_args):
        args_str = '?verb=' + verb
        if md_prefix is not None:
            args_str += '&metadataPrefix=' + md_prefix
        if optional_args:
            args_str += '&' + '&'.join('{}={}'.format(k, v) for k, v in optional_args.items())
        self._requested_url = ''.join((OAI_URL, args_str))
        self._http_response = self.fetch(self._requested_url)
        return self._http_response

    def metadata_root(self, body=None):
        body = body or self._http_response.body
        resp_xml = ET.fromstring(body)
        return resp_xml.find('./oai:GetRecord/oai:record/oai:metadata', XML_NS)

    def resumption_token(self, verb, body=None):
        body = body or self._http_response.body
        response_xml = ET.fromstring(body)
        response_token_element = response_xml.find(
            './oai:{verb}/oai:resumptionToken'.format(verb=verb), XML_NS)
        return response_token_element

    def get_app(self):
        configure.configure(self.settings_mock)
        return serve.get_app(API_VERSION, {'kuha_settings': self.settings_mock})

    def _assert_oai_response_success(self, response):
        self.assertEqual(
            response.code, 200,
            msg="Invalid HTTP response code (%d). Expecting 200. Requested url: %s"
            % (response.code, self._requested_url))
        response_xml = ET.fromstring(response.body)
        error_element = response_xml.find('./oai:error', XML_NS)
        try:
            self.assertIsNone(error_element)
        except AssertionError as exc:
            msg = '%s : OAI ERROR' % (exc.args,)
            if hasattr(error_element, 'get'):
                msg += ' code: %s' % (error_element.get('code'),)
            if hasattr(error_element, 'itertext'):
                msg += ' cdata: %s' % (''.join(error_element.itertext()),)
            raise AssertionError(msg) from exc

    def _assert_oai_header_request_attributes(self, response, expected_attrs, msg=None):
        msg = msg or 'requested url %s' % (self._requested_url,)
        response_xml = ET.fromstring(response.body)
        request_element = response_xml.find('./oai:request', XML_NS)
        self.assertEqual(request_element.attrib, expected_attrs, msg=msg)

    def _assert_oai_resumption_token(self, response, verb='ListRecords',
                                     expected_token=None, expected_attrs=None, msg=None):
        msg = msg or 'requested url %s' % (self._requested_url,)
        token_el = self.resumption_token(verb, response.body)
        self.assertEqual(''.join(token_el.itertext()), expected_token, msg)
        self.assertEqual(token_el.attrib, expected_attrs, msg)


class TestOAIRouteHandler(HandlerTestBase):
    """Test OAI responses"""

    def test_GET_request_element_contains_no_attributes_in_badVerb(self):
        url = OAI_URL + '?verb=Unknown&metadataPrefix=oai_dc'
        response = self.fetch(url)
        self._assert_oai_header_request_attributes(
            response, {},
            msg="expected no attributes in request element with url %s" % (url,))

    def test_GET_identify_returns_successful_oai_response(self):
        self._mock_query_single.side_effect = MockCoro(None)
        response = self.fetch(OAI_URL + '?verb=Identify')
        self._assert_oai_response_success(response)

    def test_GET_identify_returns_correct_datestamp(self):
        mock_study = mock.Mock()
        mock_study.get_updated.return_value = '2000-01-02T00:30:59Z'
        self._mock_query_single.side_effect = MockCoro(dummy_rval=mock_study)
        response = self.fetch(OAI_URL + '?verb=Identify')
        response_xml = ET.fromstring(response.body)
        resp_date = response_xml.find('./oai:Identify/oai:earliestDatestamp', XML_NS).text
        self.assertEqual(resp_date, '2000-01-02T00:30:59Z')

    def test_GET_getrecord_request_element_contains_no_attributes_in_badArgument(self):
        """In cases where the request that generated this response resulted in
        a badVerb or badArgument error condition, the repository must return
        the base URL of the protocol request only. Attributes must not be provided
        in these cases.

        badArgument is risen when the request includes illegal arguments, is missing
        required arguments, includes a repeated argument, or values for arguments
        have an illegal syntax.
        """
        study = Study()
        study.add_study_number('study1')
        self._mock_query_single.side_effect = MockCoro(func=_query_single(study))
        for attrs in (
                # Missing required argument
                [('metadataPrefix', 'oai_dc')],
                [('identifier', 'study1')],
                [],
                # Includes illegal arguments
                [('unknown', 'argument'),
                 ('metadataPrefix', 'oai_dc'),
                 ('identifier', 'study1')],
                # Includes a repeated argument
                [('metadataPrefix', 'oai_dc'),
                 ('identifier', 'study1'),
                 ('identifier', 'study1')]):
            with self.subTest(attrs=attrs):
                attrs.append(('verb', 'GetRecord'))
                url = OAI_URL + '?' + '&'.join(['{}={}'.format(key, value)
                                                for key, value in attrs])
                response = self.fetch(url)
                self._assert_oai_header_request_attributes(
                    response, {},
                    msg="expected no attributes in request element with url %s" % (url,))

    def test_POST_getrecord_request_element_contains_no_attributes_in_badArgument(self):
        study = Study()
        study.add_study_number('study1')
        self._mock_query_single.side_effect = MockCoro(func=_query_single(study))
        for attrs in (
                # Missing required argument
                [('metadataPrefix', 'oai_dc')],
                [('identifier', 'study1')],
                [],
                # Includes illegal arguments
                [('unknown', 'argument'),
                 ('metadataPrefix', 'oai_dc'),
                 ('identifier', 'study1')],
                # Includes a repeated argument
                [('metadataPrefix', 'oai_dc'),
                 ('identifier', 'study1'),
                 ('identifier', 'study1')],):
            with self.subTest(attrs=attrs):
                attrs.append(('verb', 'GetRecord'))
                body = '&'.join('{}={}'.format(k, v) for k, v in attrs)
                response = self.fetch(
                    OAI_URL,
                    method='POST',
                    body=body)
                self._assert_oai_header_request_attributes(
                    response, {},
                    msg="expected no attributes in request element with body %s" % (body,))

    def test_GET_getrecord_returns_successful_oai_response(self):
        testcases.KuhaUnitTestCase.setUpClass()
        dummy_study = testcases.KuhaUnitTestCase.generate_dummy_study()
        self._mock_query_single.side_effect = MockCoro(func=_query_single(dummy_study))
        self._mock_query_multiple.side_effect = MockCoro()
        for metadata_prefix in MD_PREFIXES:
            with self.subTest(metadata_prefix=metadata_prefix):
                response = self.fetch(OAI_URL + '?verb=GetRecord&metadataPrefix='
                                      '{metadata_prefix}&identifier=study_id'.format(metadata_prefix=metadata_prefix))
                self._assert_oai_response_success(response)

    def test_GET_getrecord_returns_correct_oai_header(self):
        testcases.KuhaUnitTestCase.setUpClass()
        dummy_study = testcases.KuhaUnitTestCase.generate_dummy_study()
        self._mock_query_single.side_effect = MockCoro(func=_query_single(dummy_study))
        self._mock_query_multiple.side_effect = MockCoro()
        for metadata_prefix in MD_PREFIXES:
            with self.subTest(metadata_prefix=metadata_prefix):
                response = self.fetch(OAI_URL + '?verb=GetRecord&metadataPrefix='
                                      '{metadata_prefix}&identifier=study_id'.format(metadata_prefix=metadata_prefix))
                self._assert_oai_header_request_attributes(response, {'verb': 'GetRecord',
                                                                      'identifier': 'study_id',
                                                                      'metadataPrefix': metadata_prefix})

    def test_GET_getrecord_returns_setspecs(self):
        rec = Study()
        rec.add_study_number('study_1')
        rec.add_study_titles('title', 'en')
        rec.add_study_groups('group1', 'en')
        rec.add_data_kinds('somekind', 'en')
        for metadata_prefix in MD_PREFIXES:
            with self.subTest(metadata_prefix=metadata_prefix):
                relatives = {'variables': [], 'questions': []} if metadata_prefix == 'ddi_c' else None
                response_xml = ET.fromstring(self.oai_request(rec, return_relatives=relatives,
                                                              verb='GetRecord', metadata_prefix=metadata_prefix).body)
                setspec_els = response_xml.findall('./oai:GetRecord/oai:record/oai:header/oai:setSpec', XML_NS)
                self.assertEqual(len(setspec_els), 3)
                set_specs = [''.join(x.itertext()) for x in setspec_els]
                self.assertCountEqual(set_specs, ['data_kind:somekind', 'study_groups:group1', 'language:en'])

    def test_GET_listsets_returns_successful_oai_response(self):
        self._mock_query_distinct.side_effect = MockCoro({})
        response = self.fetch(OAI_URL + '?verb=ListSets')
        self._assert_oai_response_success(response)

    def test_GET_listsets_returns_empty_sets(self):
        self._mock_query_distinct.side_effect = MockCoro({})
        response = self.fetch(OAI_URL + '?verb=ListSets')
        response_xml = ET.fromstring(response.body)
        set_els = response_xml.findall('./oai:ListSets/oai:set', XML_NS)
        self.assertEqual(len(set_els), 3)
        expected_sets = [{'spec': 'study_groups', 'name': 'Study group'},
                         {'spec': 'language', 'name': 'Language'},
                         {'spec': 'data_kind', 'name': 'Kind of data'}]
        response_sets = []
        for set_el in set_els:
            spec_els = set_el.findall('./oai:setSpec', XML_NS)
            self.assertEqual(len(spec_els), 1)
            name_els = set_el.findall('./oai:setName', XML_NS)
            self.assertEqual(len(name_els), 1)
            response_sets.append({'spec': ''.join(spec_els[0].itertext()),
                                  'name': ''.join(name_els[0].itertext())})
        self.assertEqual(response_sets, expected_sets)

    def test_GET_listsets_queries_docstore(self):
        self._mock_query_distinct.side_effect = MockCoro({})
        self.fetch(OAI_URL + '?verb=ListSets')
        self.assertEqual(self._mock_query_distinct.call_count, 3)
        self._mock_query_distinct.assert_has_calls([
            mock.call(Study, fieldname=Study.study_groups),
            mock.call(Study, fieldname=Study.study_titles.attr_language),
            mock.call(Study, fieldname=Study.data_kinds.sub_name)
        ], any_order=True)

    def test_GET_listsets_returns_available_sets(self):
        async def _query_distinct(record, fieldname=None):
            return {fieldname.path: {
                'study_groups': [
                    {'study_group': 'group_id1',
                     'name': 'first study group'},
                    {'study_group': 'group_id2',
                     'name': 'second study group'}],
                'study_titles.language': ['fi', 'en'],
                'data_kinds.data_kind': [
                    'qualitative', 'quantitative',
                    'kvalitatiivinen', 'kvantitatiivinen']
            }[fieldname.path]}
        self._mock_query_distinct.side_effect = MockCoro(func=_query_distinct)
        response = self.fetch(OAI_URL + '?verb=ListSets')
        response_xml = ET.fromstring(response.body)
        set_els = response_xml.findall('./oai:ListSets/oai:set', XML_NS)
        self.assertEqual(len(set_els), 11)
        expected = {'study_groups': ['group_id1', 'group_id2'],
                    'language': ['fi', 'en'],
                    'data_kind': ['qualitative', 'quantitative',
                                  'kvalitatiivinen', 'kvantitatiivinen']}
        response_specs = {'study_groups': [],
                          'language': [],
                          'data_kind': []}
        for set_el in set_els:
            spec_el = set_el.find('./oai:setSpec', XML_NS)
            spec_text = ''.join(spec_el.itertext())
            if ':' in spec_text:
                key, value = spec_text.split(':')
                response_specs[key].append(value)
        self.assertEqual(response_specs, expected)

    def test_GET_listsets_discards_study_group_with_no_subkey(self):
        async def _query_distinct(record, fieldname=None):
            return {fieldname.path: {
                'study_groups': [
                    {'name': 'first study group'},
                    {'study_group': 'group_id2'}],
                'study_titles.language': ['fi', 'en'],
                'data_kinds.data_kind': [
                    'qualitative', 'quantitative',
                    'kvalitatiivinen', 'kvantitatiivinen']
            }[fieldname.path]}
        self._mock_query_distinct.side_effect = MockCoro(func=_query_distinct)
        response = self.fetch(OAI_URL + '?verb=ListSets')
        response_xml = ET.fromstring(response.body)
        set_els = response_xml.findall('./oai:ListSets/oai:set', XML_NS)
        self.assertEqual(len(set_els), 10, msg=ET.dump(response_xml))
        expected = {'study_groups': ['group_id2'],
                    'language': ['fi', 'en'],
                    'data_kind': ['qualitative', 'quantitative',
                                  'kvalitatiivinen', 'kvantitatiivinen']}
        response_specs = {'study_groups': [],
                          'language': [],
                          'data_kind': []}
        for set_el in set_els:
            spec_el = set_el.find('./oai:setSpec', XML_NS)
            spec_text = ''.join(spec_el.itertext())
            if ':' in spec_text:
                key, value = spec_text.split(':')
                response_specs[key].append(value)
        self.assertEqual(response_specs, expected)

    def test_GET_listsets_discards_invalid_setspec_values(self):
        async def _query_distinct(record, fieldname=None):
            return {fieldname.path: {
                'study_groups': [
                    {'study_group': ' '}],
                'study_titles.language': ['_', '??'],
                'data_kinds.data_kind': [
                    '$', '&',
                    'ääöö', 'valid']
            }[fieldname.path]}
        self._mock_query_distinct.side_effect = MockCoro(func=_query_distinct)
        response = self.fetch(OAI_URL + '?verb=ListSets')
        response_xml = ET.fromstring(response.body)
        set_els = response_xml.findall('./oai:ListSets/oai:set', XML_NS)
        self.assertEqual(len(set_els), 5, msg=ET.dump(response_xml))
        expected = {'language': ['_'],
                    'data_kind': ['valid']}
        response_specs = {'language': [],
                          'data_kind': []}
        for set_el in set_els:
            spec_el = set_el.find('./oai:setSpec', XML_NS)
            spec_text = ''.join(spec_el.itertext())
            if ':' in spec_text:
                key, value = spec_text.split(':')
                response_specs[key].append(value)
        self.assertEqual(response_specs, expected)

    def test_GET_listmetadataformats_returns_successful_oai_response(self):
        response = self.fetch(OAI_URL + '?verb=ListMetadataFormats')
        self._assert_oai_response_success(response)

    def test_GET_listidentifiers_returns_successful_oai_response(self):
        testcases.KuhaUnitTestCase.setUpClass()
        dummy_studies = [testcases.KuhaUnitTestCase.generate_dummy_study() for _ in range(10)]
        self._mock_query_count.side_effect = MockCoro(10)
        self._mock_query_multiple.side_effect = MockCoro(func=_query_multiple({'studies': dummy_studies}))
        for metadata_prefix in MD_PREFIXES:
            with self.subTest(metadata_prefix=metadata_prefix):
                response = self.fetch(OAI_URL + '?verb=ListIdentifiers&metadataPrefix=' + metadata_prefix)
                self._assert_oai_response_success(response)

    def test_GET_listidentifiers_returns_correct_oai_header(self):
        testcases.KuhaUnitTestCase.setUpClass()
        dummy_studies = [testcases.KuhaUnitTestCase.generate_dummy_study() for _ in range(10)]
        self._mock_query_count.side_effect = MockCoro(10)
        self._mock_query_multiple.side_effect = MockCoro(func=_query_multiple({'studies': dummy_studies}))
        for metadata_prefix in MD_PREFIXES:
            with self.subTest(metadata_prefix=metadata_prefix):
                response = self.fetch(OAI_URL + '?verb=ListIdentifiers&metadataPrefix=' + metadata_prefix)
                self._assert_oai_header_request_attributes(response, {'verb': 'ListIdentifiers',
                                                                      'metadataPrefix': metadata_prefix})

    def test_GET_listidentifiers_request_unsupported_set(self):
        """Make sure server survives requests for set values containing multiple colons
        oai?verb=ListIdentifiers&metadataPrefix=oai_dc&set=study_groups:ser2:asd
        and returns noRecordsMatch
        """
        response = self.fetch(OAI_URL + '?verb=ListIdentifiers&metadataPrefix=oai_dc&'
                              'set=study_groups:ser2:asd')
        self.assertEqual(response.code, 200)
        error_element = ET.fromstring(response.body).find('.oai:error', XML_NS)
        self.assertEqual(error_element.get('code'), 'noRecordsMatch')

    def test_GET_listidentifiers_request_unsupported_set_2(self):
        """Make sure server survives request for unsupported set values with no colons
        oai?verb=ListIdentifiers&metadataPrefix=oai_dc&set=asd and returns noRecordsMatch.
        Test fix for https://bitbucket.org/tietoarkisto/kuha_oai_pmh_repo_handler/issues/19"""
        response = self.fetch(OAI_URL + '?verb=ListIdentifiers&metadataPrefix=oai_dc&'
                              'set=asd')
        self.assertEqual(response.code, 200)
        error_element = ET.fromstring(response.body).find('.oai:error', XML_NS)
        self.assertEqual(error_element.get('code'), 'noRecordsMatch')

    def _subtests_header_with_conditions(self, verb):
        current_date = datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%SZ')
        for metadata_prefix in MD_PREFIXES:
            for conditions in ({'from': current_date},
                               {'until': current_date},
                               {'set': 'language:en'},
                               {'from': current_date, 'set': 'data_kind:somekind'},
                               {'until': current_date, 'set': 'study_groups:somegroup'},
                               {'from': current_date, 'until': current_date}):
                conditions.update({'verb': verb,
                                   'metadataPrefix': metadata_prefix})
                with self.subTest(metadata_prefix=metadata_prefix, conditions=conditions):
                    q_params = '&'.join(['{key}={value}'.format(key=key, value=value)
                                         for key, value in conditions.items()])
                    response = self.fetch(OAI_URL + '?' + q_params)
                    self._assert_oai_header_request_attributes(response, conditions)

    def test_GET_listidentifiers_returns_header_with_conditions(self):
        """OAI header element should contain a request-element with attributes
        describing the request, such as URL parameters."""
        testcases.KuhaUnitTestCase.setUpClass()
        dummy_studies = [testcases.KuhaUnitTestCase.generate_dummy_study() for _ in range(10)]
        self._mock_query_count.side_effect = MockCoro(10)
        self._mock_query_multiple.side_effect = MockCoro(_query_multiple({'studies': dummy_studies}))
        self._subtests_header_with_conditions('ListIdentifiers')

    def _assert_set_calls_query_filter(self, set_param):
        self.assertEqual(self._mock_query_multiple.call_count, 1)
        c_filter = self._mock_query_multiple.call_args[1]['_filter']
        if ':' in set_param:
            set_key, set_value = set_param.split(':')
        else:
            set_key = set_param
            set_value = {'$exists': True}
        filter_key = {'data_kind': Study.data_kinds.sub_name,
                      'language': Study.study_titles.attr_language,
                      'study_groups': Study.study_groups.sub_name}[set_key]
        self.assertIn(filter_key, c_filter)
        self.assertEqual(c_filter[filter_key], set_value)

    def test_GET_listidentifiers_with_set_calls_query_with_filter(self):
        self._mock_query_count.side_effect = MockCoro(10)
        self._mock_query_multiple.side_effect = MockCoro({})
        for metadata_prefix in MD_PREFIXES:
            for set_filter in ('data_kind:somekind', 'language:en', 'study_groups:group',
                               'data_kind', 'language', 'study_groups'):
                with self.subTest(metadata_prefix=metadata_prefix,
                                  set_filter=set_filter):
                    self.fetch(OAI_URL + '?verb=ListIdentifiers&metadataPrefix={md}'
                               '&set={set_filter}'.format(md=metadata_prefix,
                                                          set_filter=set_filter))
                    self._assert_set_calls_query_filter(set_filter)
                    self._mock_query_multiple.reset_mock()

    def test_GET_listidentifiers_calls_query_with_fields(self):
        self._mock_query_count.side_effect = MockCoro(10)
        self._mock_query_multiple.side_effect = MockCoro({})
        for metadata_prefix in MD_PREFIXES:
            with self.subTest(metadata_prefix=metadata_prefix):
                self.fetch(OAI_URL + '?verb=ListIdentifiers&metadataPrefix={md}'.format(md=metadata_prefix))
                c_field_paths = [x.path for x in self._mock_query_multiple.call_args[1]['fields']]

                self.assertCountEqual(c_field_paths,
                                      [x.path for x in [Study.data_kinds,
                                                        Study.study_number,
                                                        Study.study_titles,
                                                        Study.study_groups,
                                                        Study._metadata]])

    def test_GET_listrecords_returns_successful_oai_response(self):
        testcases.KuhaUnitTestCase.setUpClass()
        dummy_recs = {'studies': [testcases.KuhaUnitTestCase.generate_dummy_study() for _ in range(10)],
                      'variables': [testcases.KuhaUnitTestCase.generate_dummy_variable() for _ in range(10)],
                      'questions': [testcases.KuhaUnitTestCase.generate_dummy_question() for _ in range(10)]}
        self._mock_query_count.side_effect = MockCoro(10)
        self._mock_query_multiple.side_effect = MockCoro(func=_query_multiple(dummy_recs))
        for metadata_prefix in MD_PREFIXES:
            with self.subTest(metadata_prefix=metadata_prefix):
                response = self.fetch(OAI_URL + '?verb=ListRecords&metadataPrefix=' + metadata_prefix)
                self._assert_oai_response_success(response)

    def test_GET_listrecords_returns_correct_oai_header(self):
        testcases.KuhaUnitTestCase.setUpClass()
        dummy_recs = {'studies': [testcases.KuhaUnitTestCase.generate_dummy_study() for _ in range(10)],
                      'variables': [testcases.KuhaUnitTestCase.generate_dummy_variable() for _ in range(10)],
                      'questions': [testcases.KuhaUnitTestCase.generate_dummy_question() for _ in range(10)]}
        self._mock_query_count.side_effect = MockCoro(10)
        self._mock_query_multiple.side_effect = MockCoro(func=_query_multiple(dummy_recs))
        for metadata_prefix in MD_PREFIXES:
            with self.subTest(metadata_prefix=metadata_prefix):
                response = self.fetch(OAI_URL + '?verb=ListRecords&metadataPrefix=' + metadata_prefix)
                self._assert_oai_header_request_attributes(response, {'verb': 'ListRecords',
                                                                      'metadataPrefix': metadata_prefix})

    def test_GET_listrecords_returns_header_with_conditions(self):
        testcases.KuhaUnitTestCase.setUpClass()
        dummy_studies = [testcases.KuhaUnitTestCase.generate_dummy_study() for _ in range(10)]
        self._mock_query_count.side_effect = MockCoro(10)
        self._mock_query_multiple.side_effect = MockCoro(
            func=_query_multiple({'studies': dummy_studies,
                                  'variables': [],
                                  'questions': []}))
        self._subtests_header_with_conditions('ListRecords')

    def test_GET_listrecords_with_set_calls_query_with_filter(self):
        self._mock_query_count.side_effect = MockCoro(10)
        self._mock_query_multiple.side_effect = MockCoro({})
        for metadata_prefix in MD_PREFIXES:
            for set_filter in ('data_kind:somekind', 'language:en', 'study_groups:group',
                               'data_kind', 'language', 'study_groups'):
                with self.subTest(metadata_prefix=metadata_prefix,
                                  set_filter=set_filter):
                    self.fetch(OAI_URL + '?verb=ListRecords&metadataPrefix={md}'
                               '&set={set_filter}'.format(md=metadata_prefix,
                                                          set_filter=set_filter))
                    self._assert_set_calls_query_filter(set_filter)
                    self._mock_query_multiple.reset_mock()

    def test_GET_listrecords_request_unsupported_set_2(self):
        """Make sure server survives request for unsupported set values with no colons
        oai?verb=ListRecords&metadataPrefix=oai_dc&set=unsupported and returns noRecordsMatch.
        Test fix for https://bitbucket.org/tietoarkisto/kuha_oai_pmh_repo_handler/issues/19"""
        response = self.fetch(OAI_URL + '?verb=ListRecords&metadataPrefix=oai_dc&'
                              'set=unsupported')
        self.assertEqual(response.code, 200)
        error_element = ET.fromstring(response.body).find('.oai:error', XML_NS)
        self.assertEqual(error_element.get('code'), 'noRecordsMatch')


class TestGetRecordDDICResponsesEmptyRecord(HandlerTestBase):
    """Test DDI-C responses content"""

    md_prefix = 'ddi_c'
    expected_field_paths = [x.path for x in [Study._metadata,
                                             Study.study_number,
                                             Study.identifiers,
                                             Study.publishers,
                                             Study.document_uris,
                                             Study.distributors,
                                             Study.copyrights,
                                             Study.study_titles,
                                             Study.parallel_titles,
                                             Study.principal_investigators,
                                             Study.study_groups,
                                             Study.publication_dates,
                                             Study.publication_years,
                                             Study.keywords,
                                             Study.time_methods,
                                             Study.sampling_procedures,
                                             Study.collection_modes,
                                             Study.analysis_units,
                                             Study.collection_periods,
                                             Study.classifications,
                                             Study.abstract,
                                             Study.study_area_countries,
                                             Study.universes,
                                             Study.data_access,
                                             Study.data_access_descriptions,
                                             Study.file_names,
                                             Study.data_kinds,
                                             Study.data_collection_copyrights,
                                             Study.citation_requirements,
                                             Study.deposit_requirements,
                                             Study.geographic_coverages,
                                             Study.related_publications,
                                             Study.instruments]]

    def setUp(self):
        super().setUp()
        self.oai_request(metadata_prefix=self.md_prefix)

    def test_calls_query_once(self):
        self.assertEqual(self._mock_query_single.call_count, 1)

    def test_calls_query_correct_record(self):
        cargs = self._mock_query_single.call_args[0]
        self.assertEqual(cargs, (Study,))

    def test_getrecord_queries_correct_filter(self):
        ckwargs = self._mock_query_single.call_args[1]
        self.assertEqual(ckwargs['_filter'], {Study.study_number: 'some_id'})

    def test_getrecord_queries_correct_fields(self):
        ckwargs = self._mock_query_single.call_args[1]
        called_fields = [x.path for x in ckwargs['fields']]
        self.assertCountEqual(called_fields, self.expected_field_paths,
                              msg="Query to Document Store contains invalid fields")


class TestGetRecordOAIDDI25ResponsesEmptyRecord(TestGetRecordDDICResponsesEmptyRecord):

    md_prefix = 'oai_ddi25'


class TestGetRecordEAD3ResponsesEmptyRecord(TestGetRecordDDICResponsesEmptyRecord):

    md_prefix = 'ead3'
    expected_field_paths = [x.path for x in [
        Study._metadata,
        Study.data_kinds,
        Study.study_number,
        Study.study_titles,
        Study.publishers,
        Study.file_names,
        Study.document_uris,
        Study.collection_periods,
        Study.principal_investigators,
        Study.keywords,
        Study.classifications,
        Study.study_area_countries,
        Study.geographic_coverages,
        Study.data_access,
        Study.data_collection_copyrights,
        Study.citation_requirements,
        Study.deposit_requirements,
        Study.abstract,
        Study.study_groups]]


class TestGetRecordOAIDCResponsesEmptyRecord(TestGetRecordDDICResponsesEmptyRecord):

    md_prefix = 'oai_dc'
    expected_field_paths = [x.path for x in [
        Study._metadata,
        Study.study_groups,
        Study.study_number,
        Study.data_kinds,
        Study.identifiers,
        Study.study_titles,
        Study.principal_investigators,
        Study.publishers,
        Study.document_uris,
        Study.abstract,
        Study.keywords,
        Study.publication_years,
        Study.study_area_countries,
        Study.data_collection_copyrights]]


class TestGetRecordOAIDCResponses(HandlerTestBase):

    _xmlns = XML_NS.copy()
    _xmlns.update({'xsi': 'http://www.w3.org/2001/XMLSchema-instance',
                   'dc': 'http://purl.org/dc/elements/1.1/',
                   'oai_dc': "http://www.openarchives.org/OAI/2.0/oai_dc/",
                   'xml': 'http://www.w3.org/XML/1998/namespace'})
    return_relatives = None
    md_prefix = 'oai_dc'

    def setUp(self):
        super().setUp()
        self.return_record = Study()
        self.return_record.add_study_number('some_id')

    def _request(self):
        self.oai_request(self.return_record, self.return_relatives, metadata_prefix=self.md_prefix)
        self._dc_root = self.metadata_root()

    def test_contains_rights(self):
        self.return_record.add_data_collection_copyrights('some right', 'en')
        self.return_record.add_data_collection_copyrights('joku oikeus', 'fi')
        expected = {'fi': 'joku oikeus',
                    'en': 'some right'}
        self._request()
        rights_els = self._dc_root.findall('oai_dc:dc/dc:rights', self._xmlns)
        self.assertEqual(len(rights_els), 2)
        for rights_el in rights_els:
            el_lang = rights_el.attrib.get('{%s}lang' % (self._xmlns['xml'],))
            el_text = ''.join(rights_el.itertext())
            self.assertEqual(expected[el_lang], el_text)


class TestGetRecordOAIDDI25Responses(HandlerTestBase):
    """Looks into the HTTP XML response"""

    _xmlns = XML_NS.copy()
    _xmlns.update({'xsi': 'http://www.w3.org/2001/XMLSchema-instance',
                   'ddi': 'ddi:codebook:2_5',
                   'xml': 'http://www.w3.org/XML/1998/namespace'})
    return_relatives = None
    md_prefix = 'oai_ddi25'

    def setUp(self):
        super().setUp()
        self.return_record = Study()
        self.return_record.add_study_number('some_id')

    def _request(self):
        self.oai_request(self.return_record, self.return_relatives, metadata_prefix=self.md_prefix)
        self._ddi_root = self.metadata_root()

    def test_contains_datakind(self):
        """Assert XML response contains correct /codeBook/stdyDscr/stdyInfo/sumDscr/dataKind"""
        self.return_record.add_data_kinds('qualitative', 'en')
        self.return_record.add_data_kinds('kvalitatiivinen', 'fi')
        self._request()
        datakind_els = self._ddi_root.findall(
            'ddi:codeBook/ddi:stdyDscr/ddi:stdyInfo/ddi:sumDscr/ddi:dataKind',
            self._xmlns)
        self.assertEqual(len(datakind_els), 2)
        expected = {'en': 'qualitative',
                    'fi': 'kvalitatiivinen'}
        for datakind_el in datakind_els:
            el_lang = datakind_el.attrib.get('{%s}lang' % (self._xmlns['xml'],))
            el_text = ''.join(datakind_el.itertext())
            self.assertEqual(expected[el_lang], el_text)

    def test_contains_serstmt(self):
        """Assert XML response contains correct /codeBook/stdyDscr/citation/serStmt"""
        self.return_record.add_study_groups('sg1', 'en', name='some group',
                                            description='some description',
                                            uri='http://some.uri')
        self.return_record.add_study_groups('sg1', 'fi', name='joku ryhmä',
                                            description='joku kuvaus',
                                            uri='http://joku.uri')
        self._request()
        serstmt_els = self._ddi_root.findall(
            'ddi:codeBook/ddi:stdyDscr/ddi:citation/ddi:serStmt',
            self._xmlns)
        self.assertEqual(len(serstmt_els), 2)
        expected = {'en': ('some group', 'some description', 'http://some.uri'),
                    'fi': ('joku ryhmä', 'joku kuvaus', 'http://joku.uri')}
        for el_index, serstmt_el in enumerate(serstmt_els):
            el_lang = serstmt_el.attrib.get('{%s}lang' % (self._xmlns['xml'],))
            # Make sure ID gets rendered only once.
            if el_index == 0:
                self.assertEqual(serstmt_el.attrib['ID'], 'sg1')
            else:
                self.assertIsNone(serstmt_el.attrib.get('ID'))
            # Test serName
            self.assertEqual(''.join(serstmt_el.find('./ddi:serName', self._xmlns).itertext()),
                             expected[el_lang][0])
            # Test serInfo
            self.assertEqual(''.join(serstmt_el.find('./ddi:serInfo', self._xmlns).itertext()),
                             expected[el_lang][1])
            # Test URI
            self.assertEqual(serstmt_el.attrib['URI'], expected[el_lang][2])

    def test_contains_stdyscr_copyright(self):
        """Assert XML response contains correct /codeBook/stdyDscr/citation/prodStmt/copyright"""
        self.return_record.add_data_collection_copyrights('joku oikeus', 'fi')
        self.return_record.add_data_collection_copyrights('some right', 'en')
        self._request()
        copyright_els = self._ddi_root.findall(
            'ddi:codeBook/ddi:stdyDscr/ddi:citation/ddi:prodStmt/ddi:copyright',
            self._xmlns)
        self.assertEqual(len(copyright_els), 2)
        expected = {'fi': 'joku oikeus',
                    'en': 'some right'}
        for copyright_el in copyright_els:
            el_lang = copyright_el.attrib.get('{%s}lang' % (self._xmlns['xml'],))
            self.assertEqual(''.join(copyright_el.itertext()), expected[el_lang])

    def test_contains_geogcover(self):
        """Assert XML response contains correct /codeBook/stdyDscr/stdyInfo/sumDscr/geogCover"""
        self.return_record.add_geographic_coverages('ensimmäinen sijainti', 'fi')
        self.return_record.add_geographic_coverages('first location', 'en')
        self.return_record.add_geographic_coverages('second location', 'en')
        self._request()
        geogcover_els = self._ddi_root.findall(
            'ddi:codeBook/ddi:stdyDscr/ddi:stdyInfo/ddi:sumDscr/ddi:geogCover',
            self._xmlns)
        self.assertEqual(len(geogcover_els), 3)
        expected_en = ['first location', 'second location']
        expected_fi = 'ensimmäinen sijainti'
        for geogcover_el in geogcover_els:
            el_lang = geogcover_el.attrib.get('{%s}lang' % (self._xmlns['xml'],))
            el_text = ''.join(geogcover_el.itertext())
            if el_lang == 'en':
                self.assertIn(el_text, expected_en)
                expected_en.remove(el_text)
            else:
                self.assertEqual(el_text, expected_fi)
        self.assertEqual(expected_en, [])

    def test_contains_citreq(self):
        self.return_record.add_citation_requirements('some citreq', 'en')
        self.return_record.add_citation_requirements('joku viittausvaatimus', 'fi')
        self._request()
        citreq_els = self._ddi_root.findall(
            'ddi:codeBook/ddi:stdyDscr/ddi:dataAccs/ddi:useStmt/ddi:citReq',
            self._xmlns)
        self.assertEqual(len(citreq_els), 2)
        expected = {'fi': 'joku viittausvaatimus',
                    'en': 'some citreq'}
        for citreq_el in citreq_els:
            el_lang = citreq_el.attrib.get('{%s}lang' % (self._xmlns['xml'],))
            el_text = ''.join(citreq_el.itertext())
            self.assertEqual(el_text, expected[el_lang])

    def test_contains_deposreq(self):
        self.return_record.add_deposit_requirements('some deposreq', 'en')
        self.return_record.add_deposit_requirements('joku käyttövaatimus', 'fi')
        self._request()
        deposreq_els = self._ddi_root.findall(
            'ddi:codeBook/ddi:stdyDscr/ddi:dataAccs/ddi:useStmt/ddi:deposReq',
            self._xmlns)
        self.assertEqual(len(deposreq_els), 2)
        expected = {'fi': 'joku käyttövaatimus',
                    'en': 'some deposreq'}
        for deposreq_el in deposreq_els:
            el_lang = deposreq_el.attrib.get('{%s}lang' % (self._xmlns['xml'],))
            el_text = ''.join(deposreq_el.itertext())
            self.assertEqual(el_text, expected[el_lang])

    def test_contains_relpubls(self):
        self.return_record.add_related_publications('some relpubl', 'en',
                                                    description='description of relpubl',
                                                    distribution_date='2000-01-01',
                                                    uri='http://some.uri')
        self.return_record.add_related_publications('joku relpubl', 'fi',
                                                    description='relpubl kuvaus',
                                                    distribution_date='2001-12-31',
                                                    uri='http://joku.uri')
        self._request()
        expected_els = {'some relpubl': ('en', 'description of relpubl',
                                         '2000-01-01', 'http://some.uri'),
                        'joku relpubl': ('fi', 'relpubl kuvaus',
                                         '2001-12-31', 'http://joku.uri')}
        relpubl_els = self._ddi_root.findall(
            'ddi:codeBook/ddi:stdyDscr/ddi:othrStdyMat/ddi:relPubl', self._xmlns)
        self.assertEqual(len(relpubl_els), len(expected_els))
        for relpubl_el in relpubl_els:
            title = ''.join(relpubl_el.find('./ddi:citation/ddi:titlStmt/ddi:titl', self._xmlns).itertext())
            self.assertIn(title, expected_els)
            exp_lang, exp_desc, exp_date, exp_uri = expected_els.pop(title)
            date = relpubl_el.find('./ddi:citation/ddi:distStmt/ddi:distDate', self._xmlns).get('date')
            self.assertEqual(date, exp_date)
            lang = relpubl_el.get('{%s}lang' % (self._xmlns['xml'],))
            self.assertEqual(lang, exp_lang)
            uri = relpubl_el.find('./ddi:citation/ddi:holdings', self._xmlns).get('URI')
            self.assertEqual(uri, exp_uri)
            desc = element_strip_descendant_text(relpubl_el)
            self.assertEqual(desc, exp_desc)

    def test_contains_combines_relpubl_if_descriptions_are_equal(self):
        self.return_record.add_related_publications('some relpubl', 'en',
                                                    description='description of relpubl',
                                                    distribution_date='2000-01-01',
                                                    uri='http://some.uri')
        self.return_record.add_related_publications('some another relpubl', 'en',
                                                    description='description of relpubl',
                                                    distribution_date='2001-12-31',
                                                    uri='http://another.uri')
        self.return_record.add_related_publications('yet another relpubl', 'fi',
                                                    description='relpubl kuvaus',
                                                    distribution_date='2002-12-31',
                                                    uri='http://joku.uri')
        self._request()
        expected_els = {'description of relpubl': {
            'some relpubl': ('en', '2000-01-01', 'http://some.uri'),
            'some another relpubl': ('en', '2001-12-31', 'http://another.uri')},
                        'relpubl kuvaus': {
            'yet another relpubl': ('fi', '2002-12-31', 'http://joku.uri')}}
        relpubl_els = self._ddi_root.findall(
            'ddi:codeBook/ddi:stdyDscr/ddi:othrStdyMat/ddi:relPubl', self._xmlns)
        self.assertEqual(len(relpubl_els), len(expected_els))
        for relpubl_el in relpubl_els:
            desc = element_strip_descendant_text(relpubl_el)
            self.assertIn(desc, expected_els)
            for cit_el in relpubl_el.findall('./ddi:citation', self._xmlns):
                title = ''.join(cit_el.find('./ddi:titlStmt/ddi:titl', self._xmlns).itertext())
                self.assertIn(title, expected_els[desc])
                exp_lang, exp_date, exp_uri = expected_els[desc].pop(title)
                date = cit_el.find('./ddi:distStmt/ddi:distDate', self._xmlns).get('date')
                self.assertEqual(date, exp_date)
                lang = relpubl_el.get('{%s}lang' % (self._xmlns['xml'],))
                self.assertEqual(lang, exp_lang)
                uri = cit_el.find('./ddi:holdings', self._xmlns).get('URI')
                self.assertEqual(uri, exp_uri)

    def test_IDs_are_unique(self):
        """Assert template discards duplicate ID-attribute values
        regardless of the element

        serStmt gets rendered first, it will have an ID,
        keyword & topcClas will not.
        """
        self.return_record.add_study_groups('ID1', 'en')
        self.return_record.add_keywords('ID1', 'en')
        self.return_record.add_classifications('ID1', 'en')
        self._request()
        serStmt_els = self._ddi_root.findall(
            'ddi:codeBook/ddi:stdyDscr/ddi:citation/ddi:serStmt',
            self._xmlns)
        self.assertEqual(len(serStmt_els), 1)
        self.assertEqual(serStmt_els.pop().attrib.get('ID'), 'ID1')
        keyword_els = self._ddi_root.findall(
            'ddi:codeBook/ddi:stdyDscr/ddi:stdyInfo/ddi:subject/ddi:keyword',
            self._xmlns)
        self.assertEqual(len(keyword_els), 1)
        self.assertNotIn('ID', keyword_els.pop().attrib)
        topcclas_els = self._ddi_root.findall(
            'ddi:codeBook/ddi:stdyDscr/ddi:stdyInfo/ddi:subject/ddi:topcClas',
            self._xmlns)
        self.assertEqual(len(topcclas_els), 1)
        self.assertNotIn('ID', topcclas_els.pop().attrib)


class TestGetRecordDDICResponses(TestGetRecordOAIDDI25Responses):
    """Looks into the HTTP XML response"""

    md_prefix = 'ddi_c'
    return_relatives = {'variables': [],
                        'questions': []}

    def test_IDs_are_unique_in_var_and_question(self):
        # FORMAT
        self.return_record.add_classifications('ID1', 'en')
        var1, var2 = Variable(), Variable()
        self.return_relatives['variables'] = [var1, var2]
        var1.add_variable_name('var1')
        var1.add_codelist_codes('ID3', 'fi', label='toinen label', missing=False)
        var1.add_codelist_codes('ID3', 'en', label='another label', missing=False)
        var2.add_variable_name('var2')
        var2.add_codelist_codes('ID2', 'en', label='somelabel', missing=True)
        var2.add_codelist_codes('ID2', 'fi', label='jokulabel', missing=True)
        qstn1, qstn2 = Question(), Question()
        qstn1.add_question_identifier('ID1')
        qstn2.add_question_identifier('ID2')
        qstn1.add_variable_name('var1')
        qstn2.add_variable_name('var1')
        self.return_relatives['questions'] = [qstn1, qstn2]
        # CALL
        self._request()
        # ASSERT
        var_els = self._ddi_root.findall(
            'ddi:codeBook/ddi:dataDscr/ddi:var',
            self._xmlns)
        self.assertEqual(len(var_els), 2)
        var1_el, var2_el = var_els
        qstn_els = var1_el.findall('./ddi:qstn',
                                   self._xmlns)
        self.assertEqual(len(qstn_els), 2)
        qstn1_el, qstn2_el = qstn_els
        self.assertNotIn('ID', qstn1_el.attrib)
        self.assertEqual(qstn2_el.attrib['ID'], 'ID2')
        var1_catgry_els = var1_el.findall('./ddi:catgry',
                                          self._xmlns)
        self.assertEqual(len(var1_catgry_els), 1)
        self.assertEqual(var1_catgry_els.pop().attrib['ID'], 'ID3')
        var2_catgry_els = var2_el.findall('./ddi:catgry',
                                          self._xmlns)
        self.assertEqual(len(var2_catgry_els), 1)
        self.assertNotIn('ID', var2_catgry_els.pop().attrib)


class TestGetRecordEAD3Responses(HandlerTestBase):

    _xmlns = XML_NS.copy()
    _xmlns.update({'xsi': 'http://www.w3.org/2001/XMLSchema-instance',
                   'ead3': 'http://ead3.archivists.org/schema/',
                   'xml': 'http://www.w3.org/XML/1998/namespace'})
    return_relatives = None
    md_prefix = 'ead3'

    def setUp(self):
        super().setUp()
        self.return_record = Study()
        self.return_record.add_study_number('some_id')

    def _request(self):
        self.oai_request(self.return_record, self.return_relatives, metadata_prefix=self.md_prefix)
        self._ead_root = self.metadata_root()

    def test_contains_series_component(self):
        # FORMAT
        self.return_record.add_study_groups('serie_id', 'en', name='some serie',
                                            description='some description',
                                            uri='http://some.uri')
        self.return_record.add_study_groups('serie_id', 'fi', name='joku sarja',
                                            description='joku kuvaus',
                                            uri='http://joku.uri')
        # CALL
        self._request()
        # Assert
        c01_els = self._ead_root.findall('ead3:ead/ead3:archdesc/ead3:dsc/ead3:c01',
                                         self._xmlns)
        self.assertEqual(len(c01_els), 1)
        c01_el = c01_els.pop()
        self.assertEqual(c01_el.attrib, {'level': 'series'})
        unitid_els = c01_el.findall('ead3:did/ead3:unitid', self._xmlns)
        self.assertEqual(len(unitid_els), 1)
        unitid_el = unitid_els.pop()
        self.assertEqual(unitid_el.attrib['identifier'], 'serie_id')
        self.assertEqual(''.join(unitid_el.itertext()), 'serie_id')
        unittitle_els = c01_el.findall('ead3:did/ead3:unittitle', self._xmlns)
        self.assertEqual(len(unittitle_els), 2)
        expected = {'en': 'some serie',
                    'fi': 'joku sarja'}
        for unittitle_el in unittitle_els:
            el_lang = unittitle_el.attrib.get('lang')
            el_text = ''.join(unittitle_el.itertext())
            self.assertEqual(el_text, expected[el_lang])
        dao_els = c01_el.findall('ead3:did/ead3:daoset/ead3:dao', self._xmlns)
        expected = {'fi': 'http://joku.uri',
                    'en': 'http://some.uri'}
        for dao_el in dao_els:
            el_lang = dao_el.attrib.get('lang')
            self.assertEqual(dao_el.attrib.get('href'), expected[el_lang])
        self.assertEqual(len(dao_els), 2)
        scopecontent_els = c01_el.findall('ead3:scopecontent', self._xmlns)
        self.assertEqual(len(scopecontent_els), 2)
        expected = {'fi': 'joku kuvaus',
                    'en': 'some description'}
        for scopecontent_el in scopecontent_els:
            el_lang = scopecontent_el.attrib.get('lang')
            self.assertEqual(len(scopecontent_el.findall('ead3:p', self._xmlns)), 1)
            el_text = ''.join(scopecontent_el.find('ead3:p', self._xmlns).itertext())
            self.assertEqual(el_text, expected[el_lang])

    def test_series_component_has_no_daoset_if_single_uri(self):
        self.return_record.add_study_groups('serie_id', 'en', uri='http://some.uri')
        self._request()
        c01_els = self._ead_root.findall('ead3:ead/ead3:archdesc/ead3:dsc/ead3:c01',
                                         self._xmlns)
        self.assertEqual(len(c01_els), 1)
        c01_el = c01_els.pop()
        self.assertEqual(c01_el.findall('ead3:did/ead3:daoset', self._xmlns), [])
        dao_els = c01_el.findall('ead3:did/ead3:dao', self._xmlns)
        self.assertEqual(len(dao_els), 1)
        self.assertEqual(dao_els[0].attrib, {'href': 'http://some.uri',
                                             'daotype': 'unknown',
                                             'lang': 'en'})

    def test_geogname_with_geographic_coverages(self):
        # FORMAT
        self.return_record.add_geographic_coverages('first location', 'en')
        self.return_record.add_geographic_coverages('second location', 'en')
        self.return_record.add_geographic_coverages('third location', 'en')
        self.return_record.add_geographic_coverages('eka sijainti', 'fi')
        self.return_record.add_geographic_coverages('toka sijainti', 'fi')
        self.return_record.add_geographic_coverages('kolmas sijainti', 'fi')
        # CALL
        self._request()
        # ASSERT
        geogname_els = self._ead_root.findall(
            'ead3:ead/ead3:archdesc/ead3:dsc/ead3:c01/ead3:c02/ead3:controlaccess/ead3:geogname',
            self._xmlns)
        self.assertEqual(len(geogname_els), 6)
        expected = {'fi': ['eka sijainti', 'toka sijainti', 'kolmas sijainti'],
                    'en': ['first location', 'second location', 'third location']}
        for geogname_el in geogname_els:
            part_els = geogname_el.findall('ead3:part', self._xmlns)
            self.assertEqual(len(part_els), 1)
            part_el = part_els.pop()
            el_lang = part_el.attrib.get('lang')
            el_text = ''.join(part_el.itertext())
            self.assertIn(el_text, expected[el_lang])
            expected[el_lang].remove(el_text)
        self.assertEqual(expected, {'fi': [], 'en': []}, msg='Did not find all expected values')

    def test_accessrestrict_with_data_access(self):
        self.return_record.add_data_access('some restriction', 'en')
        self.return_record.add_data_access('joku rajoitus', 'fi')
        self._request()
        accessrestrict_els = self._ead_root.findall(
            'ead3:ead/ead3:archdesc/ead3:dsc/ead3:c01/ead3:c02/ead3:accessrestrict',
            self._xmlns)
        self.assertEqual(len(accessrestrict_els), 2)
        expected = {'fi': 'joku rajoitus',
                    'en': 'some restriction'}
        for accessrestrict_el in accessrestrict_els:
            p_els = accessrestrict_el.findall('ead3:p', self._xmlns)
            self.assertEqual(len(p_els), 1)
            p_el = p_els.pop()
            el_lang = p_el.attrib.get('lang')
            el_text = ''.join(p_el.itertext())
            self.assertEqual(el_text, expected[el_lang])

    def test_accessrestrict_with_data_coll_copyrights(self):
        self.return_record.add_data_collection_copyrights('some copyright', 'en')
        self.return_record.add_data_collection_copyrights('joku copyright', 'fi')
        self._request()
        accessrestrict_els = self._ead_root.findall(
            'ead3:ead/ead3:archdesc/ead3:dsc/ead3:c01/ead3:c02/ead3:accessrestrict',
            self._xmlns)
        self.assertEqual(len(accessrestrict_els), 2)
        expected = {'fi': 'joku copyright',
                    'en': 'some copyright'}
        for accessrestrict_el in accessrestrict_els:
            p_els = accessrestrict_el.findall('ead3:p', self._xmlns)
            self.assertEqual(len(p_els), 1)
            p_el = p_els.pop()
            el_lang = p_el.attrib.get('lang')
            el_text = ''.join(p_el.itertext())
            self.assertEqual(el_text, expected[el_lang])

    def test_userestrict_with_citation_requirements(self):
        self.return_record.add_citation_requirements('some citreq', 'en')
        self.return_record.add_citation_requirements('joku viittausvaatimus', 'fi')
        self._request()
        userestrict_els = self._ead_root.findall(
            'ead3:ead/ead3:archdesc/ead3:dsc/ead3:c01/ead3:c02/ead3:userestrict',
            self._xmlns)
        self.assertEqual(len(userestrict_els), 2)
        expected = {'fi': 'joku viittausvaatimus',
                    'en': 'some citreq'}
        for userestrict_el in userestrict_els:
            p_els = userestrict_el.findall('ead3:p', self._xmlns)
            self.assertEqual(len(p_els), 1)
            p_el = p_els.pop()
            el_lang = p_el.attrib.get('lang')
            el_text = ''.join(p_el.itertext())
            self.assertEqual(el_text, expected[el_lang])

    def test_userestrict_with_deposit_requirements(self):
        self.return_record.add_deposit_requirements('some deposit requirement', 'en')
        self.return_record.add_deposit_requirements('joku talletusvaatimus', 'fi')
        self._request()
        userestrict_els = self._ead_root.findall(
            'ead3:ead/ead3:archdesc/ead3:dsc/ead3:c01/ead3:c02/ead3:userestrict',
            self._xmlns)
        self.assertEqual(len(userestrict_els), 2)
        expected = {'fi': 'joku talletusvaatimus',
                    'en': 'some deposit requirement'}
        for userestrict_el in userestrict_els:
            p_els = userestrict_el.findall('ead3:p', self._xmlns)
            self.assertEqual(len(p_els), 1)
            p_el = p_els.pop()
            el_lang = p_el.attrib.get('lang')
            el_text = ''.join(p_el.itertext())
            self.assertEqual(el_text, expected[el_lang])

    def _assert_in_element_parts(self, elements, expected):
        self.assertEqual(len(elements), len(expected))
        for element in elements:
            part_els = element.findall('ead3:part', self._xmlns)
            self.assertEqual(len(part_els), 1)
            lookup_val = (part_els[0].get('lang'), ''.join(part_els[0].itertext()))
            self.assertIn(lookup_val, expected)
            expected.remove(lookup_val)
        self.assertEqual(expected, [])

    def _assert_corpnames(self, expected_corpnames):
        corpname_els = self._ead_root.findall(
            'ead3:ead/ead3:archdesc/ead3:dsc/ead3:c01/ead3:c02/ead3:controlaccess/ead3:corpname',
            self._xmlns)
        self._assert_in_element_parts(corpname_els, expected_corpnames)

    def _assert_persnames(self, expected_persnames):
        persname_els = self._ead_root.findall(
            'ead3:ead/ead3:archdesc/ead3:dsc/ead3:c01/ead3:c02/ead3:controlaccess/ead3:persname',
            self._xmlns)
        self._assert_in_element_parts(persname_els, expected_persnames)

    def test_no_pricipal_investigators(self):
        self._request()
        self._assert_corpnames([])
        self._assert_persnames([])

    def test_principal_investigator_with_organization(self):
        self.return_record.add_principal_investigators('some pi', 'en', organization='some org')
        self.return_record.add_principal_investigators('joku pi', 'fi', organization='joku org')
        self._request()
        self._assert_corpnames([('fi', 'joku org'), ('en', 'some org')])
        self._assert_persnames([('fi', 'joku pi'), ('en', 'some pi')])

    def test_principal_investigator_without_organization(self):
        self.return_record.add_principal_investigators('some pi', 'en')
        self.return_record.add_principal_investigators('joku pi', 'fi')
        self._request()
        self._assert_corpnames([('fi', 'joku pi'), ('en', 'some pi')])
        self._assert_persnames([])

    def test_principal_investigator_mixed(self):
        """mixed principal investigators with and without organization"""
        self.return_record.add_principal_investigators('some pi', 'en', organization='some org')
        self.return_record.add_principal_investigators('another pi', 'en')
        self._request()
        self._assert_corpnames([('en', 'some org'), ('en', 'another pi')])
        self._assert_persnames([('en', 'some pi')])


class FakeDatetime(datetime.datetime):
    """Class datetime.datetime methods cannot be mocked
    without touching the container class. We just need the date
    to stay consistent while testing.
    """

    @classmethod
    def utcnow(cls):
        return cls(2019, 12, 12, 7, 14, 37, 685563)


class TestListRecordsResumptionTokens(HandlerTestBase):

    RES_COUNT = 5

    def setUp(self):
        self.settings_mock.oai_pmh_results_per_list = self.RES_COUNT
        super().setUp()
        # FORMAT & MOCK
        self._complete_list_size = 20
        testcases.KuhaUnitTestCase.setUpClass()
        return_records = {
            'studies': [
                testcases.KuhaUnitTestCase.generate_dummy_study() for _ in range(self.RES_COUNT)],
            'variables': [],
            'questions': []}
        self._mock_query_count.side_effect = MockCoro(dummy_rval=self._complete_list_size)
        self._mock_query_multiple.side_effect = MockCoro(func=_query_multiple(return_records))

    def _mock_out_protocol_datetime(self):
        self._now = FakeDatetime.utcnow()
        self._init_patcher(mock.patch.object(protocol_datetime, 'datetime', FakeDatetime, spec=datetime.datetime))

    def test_returns_resumptionToken_for_incomplete_list(self):
        self._mock_out_protocol_datetime()
        for md_prefix in MD_PREFIXES:
            with self.subTest(md_prefix=md_prefix):
                # CALL
                response = self.oai_list_request(md_prefix=md_prefix)
                # ASSERT
                # encode expected token
                expected_token = ('cursor:0&from:None&until:{}&set:None&completeListSize:{}&'
                                  'metadataPrefix:{}'.
                                  format(as_supported_datestring(self._now),
                                         self._complete_list_size, md_prefix))
                self._assert_oai_resumption_token(
                    response,
                    expected_token=encode_uri(expected_token),
                    expected_attrs={'completeListSize': str(self._complete_list_size),
                                    'cursor': '0'})

    def test_returns_resumptionToken_with_set(self):
        self._mock_out_protocol_datetime()
        for md_prefix in MD_PREFIXES:
            with self.subTest(md_prefix=md_prefix):
                response = self.oai_list_request(
                    md_prefix=md_prefix,
                    set='data_kind:somekind'
                )
                expected_token = ('cursor:0&from:None&until:{}&set:data_kind:somekind&'
                                  'completeListSize:{}&metadataPrefix:{}'.
                                  format(as_supported_datestring(self._now),
                                         self._complete_list_size, md_prefix))
                self._assert_oai_resumption_token(
                    response,
                    expected_token=encode_uri(expected_token),
                    expected_attrs={'completeListSize': str(self._complete_list_size),
                                    'cursor': '0'})

    def test_returns_resumptionToken_with_set_and_datestamps(self):
        for md_prefix in MD_PREFIXES:
            with self.subTest(md_prefix=md_prefix):
                response = self.oai_list_request(
                    **{'md_prefix': md_prefix,
                       'set': 'data_kind:somekind',
                       'from': '2000-12-31T12:31:59Z',
                       'until': '2001-01-01T00:00:00Z'}
                )
                expected_token = ('cursor:0&from:2000-12-31T12:31:59Z&'
                                  'until:2001-01-01T00:00:00Z&set:data_kind:somekind&'
                                  'completeListSize:{}&metadataPrefix:{}'.
                                  format(self._complete_list_size, md_prefix))
                self._assert_oai_resumption_token(
                    response,
                    expected_token=encode_uri(expected_token),
                    expected_attrs={'completeListSize': str(self._complete_list_size),
                                    'cursor': '0'})

    def test_calls_query_count_with_set_from_requested_resumptiontoken(self):
        """Make sure set argument from resumptiontoken is used in queries"""
        # first request to get resumptiontoken
        self.oai_list_request(verb='ListRecords', set='data_kind:somekind')
        self.assertEqual(self._mock_query_count.call_count, 1)
        kwargs = self._mock_query_count.call_args[1]
        self.assertIn('_filter', kwargs)
        self.assertIn(Study.data_kinds.sub_name, kwargs['_filter'])
        self.assertEqual(kwargs['_filter'][Study.data_kinds.sub_name], 'somekind')
        token_str = ''.join(self.resumption_token('ListRecords').itertext())
        # second request using resumptiontoken
        self.oai_list_request(verb='ListRecords', md_prefix=None, resumptionToken=token_str)
        self.assertEqual(self._mock_query_count.call_count, 2)
        kwargs = self._mock_query_count.call_args[1]
        self.assertIn('_filter', kwargs)
        self.assertIn(Study.data_kinds.sub_name, kwargs['_filter'])
        self.assertEqual(kwargs['_filter'][Study.data_kinds.sub_name], 'somekind')

    def test_calls_query_with_set_and_datestamps_seconds_precision(self):
        """Query with correct set & datestamp info.

        Until-datestamp gets incremented with the smallest increment step calculated
        by datestamp granularity: '2001-01-01T00:00:00Z' -> '2001-01-01T00:00:01Z'.
        This is due to the fact that queries by document store to MongoDB are executed
        in milliseconds precision. To get inclusive query "less than or equal to
        '2001-01-01T00:00:00Z'", we actually perform exclusive query with
        '2001-01-01T00:00:01Z', to get all records that are updated within
        '2001-01-01T00:00:00Z' but before '2001-01-01T00:00:01Z'.
        """
        # MOCK & FORMAT
        expected_filter = {
            Study.data_kinds.sub_name: 'somekind',
            Study._metadata.attr_updated: {
                '$gte': datetime.datetime(2000, 12, 31, 12, 31, 59),
                '$lt': datetime.datetime(2001, 1, 1, 0, 0, 1)}}
        # CALL
        self.oai_list_request(
            **{'set': 'data_kind:somekind',
               'from': '2000-12-31T12:31:59Z',
               'until': '2001-01-01T00:00:00Z'}
        )
        # ASSERT
        self.assertEqual(self._mock_query_count.call_count, 1)
        kwargs = self._mock_query_count.call_args[1]
        self.assertIn('_filter', kwargs)
        self.assertEqual(kwargs['_filter'], expected_filter)
        self.assertEqual(self._mock_query_multiple.call_count, 1)
        # Make sure the same filter is applied to query_multiple
        kwargs_query_multiple = self._mock_query_multiple.call_args[1]
        self.assertEqual(kwargs_query_multiple['_filter'], kwargs['_filter'])

    def test_calls_query_count_with_set_and_datestamps_days_precision(self):
        # FORMAT
        expected_filter = {
            Study.data_kinds.sub_name: 'somekind',
            Study._metadata.attr_updated: {
                '$gte': None,
                '$lt': datetime.datetime(2001, 1, 2)
            }}
        # CALL
        self.oai_list_request(
            **{'set': 'data_kind:somekind',
               'until': '2001-01-01'}
        )
        # ASSERT
        self.assertEqual(self._mock_query_count.call_count, 1)
        kwargs = self._mock_query_count.call_args[1]
        self.assertIn('_filter', kwargs)
        self.assertEqual(kwargs['_filter'], expected_filter)

    def test_calls_query_count_with_set_and_datestamps_from_requested_resumptiontoken(self):
        """Correct queries for full list request sequence"""
        kwargs = {'set': 'data_kind:somekind',
                  'from': '2000-12-31T12:00:00Z',
                  'until': '2001-01-01T00:00:00Z'}
        for multiplier in range(int(self._complete_list_size / self.RES_COUNT)):
            msg = "Round: " + str(multiplier + 1)
            expected_filter = {
                Study._metadata.attr_updated: {
                    '$gte': datetime.datetime(2000, 12, 31, 12, 0, 0),
                    '$lt': datetime.datetime(2001, 1, 1, 0, 0, 1)},
                Study.data_kinds.sub_name: 'somekind'}
            expected_limit = self.RES_COUNT
            expected_skip = 0 + (self.RES_COUNT * multiplier)
            # CALL
            self.oai_list_request(verb='ListIdentifiers', **kwargs)
            # ASSERT
            msg += ". URL: " + self._requested_url
            kwargs['resumptionToken'] = ''.join(
                self.resumption_token('ListIdentifiers').itertext())
            self.assertEqual(self._mock_query_multiple.call_count, multiplier + 1, msg=msg)
            cargs, ckwargs = self._mock_query_multiple.call_args
            self.assertEqual(cargs, (Study,), msg=msg)
            self.assertEqual(ckwargs['_filter'], expected_filter, msg=msg)
            self.assertEqual(ckwargs['limit'], expected_limit, msg=msg)
            self.assertEqual(ckwargs['skip'], expected_skip, msg=msg)

    def test_calls_query_multiple_with_set(self):
        """Correct calls to queries for full list request sequence"""
        # Using oai_dc here so we don't get queries to relative records.
        # MOCK & FORMAT
        self._mock_out_protocol_datetime()
        kwargs = {'set': 'data_kind:somekind'}
        for multiplier in range(int(self._complete_list_size / self.RES_COUNT)):
            msg = "Round: " + str(multiplier + 1)
            expected_filter = {
                Study._metadata.attr_updated: {
                    '$gte': None,
                    '$lt': (self._now -
                            datetime.timedelta(microseconds=self._now.microsecond) +
                            datetime.timedelta(seconds=1))},
                Study.data_kinds.sub_name: 'somekind'}
            expected_limit = self.RES_COUNT
            expected_skip = 0 + (self.RES_COUNT * multiplier)
            # CALL
            self.oai_list_request(verb='ListIdentifiers', **kwargs)
            # ASSERT
            msg += ". URL: " + self._requested_url
            kwargs['resumptionToken'] = ''.join(
                self.resumption_token('ListIdentifiers').itertext())
            self.assertEqual(self._mock_query_multiple.call_count, multiplier + 1, msg=msg)
            cargs, ckwargs = self._mock_query_multiple.call_args
            self.assertEqual(cargs, (Study,), msg=msg)
            self.assertEqual(ckwargs['_filter'], expected_filter, msg=msg)
            self.assertEqual(ckwargs['limit'], expected_limit, msg=msg)
            self.assertEqual(ckwargs['skip'], expected_skip, msg=msg)


if __name__ == '__main__':
    testing.main()
