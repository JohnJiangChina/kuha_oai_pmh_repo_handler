import io
import tempfile
import urllib.parse
from unittest import mock
from kuha_common.testing import testcases
from kuha_oai_pmh_repo_handler import list_records


def _valid_response_body(ident, token=''):
    ident_xml = '<identifier>%s</identifier>' % (ident,)
    if token is None:
        token_xml = ''
    elif token == '':
        token_xml = '<resumptionToken />'
    else:
        token_xml = ('<resumptionToken completeListSize="1" cursor="0">'
                     + token +
                     '</resumptionToken>')
    return ('<?xml version="1.0" encoding="UTF-8"?>'
            '<OAI-PMH xmlns="http://www.openarchives.org/OAI/2.0/" '
            'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" '
            'xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/ '
            'http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd">'
            '<responseDate>2020-03-16T12:24:51Z</responseDate>'
            '<request verb="ListRecords" metadataPrefix="oai_dc">'
            'http://some.url</request>'
            '<ListRecords>'
            '<record>'
            '<header>'
            + ident_xml +
            '<datestamp>2020-03-12T23:00:07Z</datestamp>'
            '<setSpec>language:en</setSpec><setSpec>language:fi</setSpec>'
            '</header>'
            '<metadata />'
            '</record>'
            + token_xml +
            '</ListRecords>'
            '</OAI-PMH>')


def _oaierror_response_body():
    return ('<?xml version="1.0" encoding="UTF-8"?>'
            '<OAI-PMH xmlns="http://www.openarchives.org/OAI/2.0/" '
            'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" '
            'xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/ '
            'http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd">'
            '<responseDate>2020-03-16T12:24:51Z</responseDate>'
            '<request verb="ListRecords" metadataPrefix="oai_dc">'
            'http://some.url</request>'
            '<error code="cannotDisseminateFormat">'
            'Metadata format not supported by this repository'
            '</error></OAI-PMH>')


def _get_cli_args_mock(metadata_prefix, **cli_args):
    args = {'oai_set': None,
            'oai_until': None,
            'oai_from': None,
            'base_url': 'http://localhost:6003/v0/oai',
            'request_timeout': 30,
            'loglevel': 'INFO',
            'output': None,
            'metadata_prefix': metadata_prefix}
    args.update(cli_args)
    mock_args = mock.Mock()
    mock_args.configure_mock(**args)
    return mock_args


def _extract_exception_loglines(cm_log):
    loglines = cm_log.output[0].splitlines()
    return loglines[0], loglines[1], loglines[-1]


class TestCliRuns(testcases.KuhaUnitTestCase):

    def setUp(self):
        super().setUp()
        self._mock_HTTPClient = self.init_patcher(
            mock.patch.object(list_records, 'HTTPClient'))
        self._mock_cli_args = self.init_patcher(
            mock.patch.object(list_records, '_cli_args'))

    def _set_cli_args(self, *args, **kwargs):
        mock_args = _get_cli_args_mock(*args, **kwargs)
        self._mock_cli_args.return_value = mock_args

    def test_reads_mandatory_args(self):
        # Mock & format
        self._set_cli_args('some_prefix',
                           base_url='http://some.url',
                           request_timeout=55)
        self._mock_HTTPClient.return_value.fetch.side_effect = [
            mock.Mock(code=200, body=_valid_response_body('some_id1', 'some-opaque-token')),
            mock.Mock(code=200, body=_valid_response_body('some_id2'))]
        # Call
        list_records.main()
        # Assert
        self.assertEqual(self._mock_HTTPClient.return_value.fetch.call_count, 2)
        self._mock_HTTPClient.return_value.fetch.assert_has_calls([
            mock.call('http://some.url?verb=ListRecords&metadataPrefix=some_prefix',
                      request_timeout=55),
            mock.call('http://some.url?verb=ListRecords&resumptionToken=some-opaque-token',
                      request_timeout=55)])

    def test_reads_optional_args(self):
        self._set_cli_args('md_prefix',
                           oai_set='language:en',
                           oai_until='2020-03-16T12:00:00Z',
                           oai_from='2020-03-16T00:00:00Z',
                           base_url='http://base.url',
                           request_timeout=40,
                           loglevel='INFO',
                           output=None)
        self._mock_HTTPClient.return_value.fetch.side_effect = [
            mock.Mock(code=200, body=_valid_response_body('some_id1', 'some-opaque-token')),
            mock.Mock(code=200, body=_valid_response_body('some_id2'))]
        # Call
        list_records.main()
        # Assert
        expected = (
            (
                {'base': 'http://base.url',
                 'qs': {
                     'verb': ['ListRecords'],
                     'metadataPrefix': ['md_prefix'],
                     'set': ['language:en'],
                     'from': ['2020-03-16T00:00:00Z'],
                     'until': ['2020-03-16T12:00:00Z']}},
                {'request_timeout': 40}),
            (
                {'base': 'http://base.url',
                 'qs': {
                     'verb': ['ListRecords'],
                     'resumptionToken': ['some-opaque-token']}},
                {'request_timeout': 40}))
        calls = self._mock_HTTPClient.return_value.fetch.call_args_list
        self.assertEqual(len(calls), 2)
        for index, call in enumerate(calls):
            cargs, ckwargs = call
            eurl, ekwargs = expected[index]
            self.assertEqual(len(cargs), 1)
            curl = cargs[0]
            cbase, cqs = curl.split('?')
            self.assertEqual(cbase, eurl['base'])
            cqs_dict = urllib.parse.parse_qs(cqs)
            self.assertEqual(cqs_dict, eurl['qs'])
            self.assertEqual(ckwargs, ekwargs)

    @mock.patch('sys.stdout', new_callable=io.StringIO)
    def test_outputs_identifiers_to_stdout(self, mock_stdout):
        self._set_cli_args('some_prefix')
        self._mock_HTTPClient.return_value.fetch.side_effect = [
            mock.Mock(code=200, body=_valid_response_body('some_id1', 'some-opaque-token')),
            mock.Mock(code=200, body=_valid_response_body('some_id2'))]
        # Call
        list_records.main()
        self.assertEqual(
            mock_stdout.getvalue(), 'Found 2 distinct records\nsome_id1\nsome_id2\n')

    def test_outputs_identifiers_to_file(self):
        outfile = tempfile.NamedTemporaryFile()
        self._set_cli_args('some_prefix',
                           output=outfile.name)
        self._mock_HTTPClient.return_value.fetch.side_effect = [
            mock.Mock(code=200, body=_valid_response_body('some_id1', 'token')),
            mock.Mock(code=200, body=_valid_response_body('some_id2', 'token')),
            mock.Mock(code=200, body=_valid_response_body('some_id3'))]
        list_records.main()
        expected = [b'Found 3 distinct records\n', b'some_id1\n',
                    b'some_id2\n', b'some_id3\n']
        self.assertEqual(outfile.readlines(), expected)

    def test_no_resumptiontoken_logs_warning(self):
        self._set_cli_args('some_prefix')
        self._mock_HTTPClient.return_value.fetch.side_effect = [
            mock.Mock(code=200, body=_valid_response_body('some_id', None))]
        with self.assertLogs(level='WARNING') as log_cm:
            list_records.main()
        self.assertEqual(
            log_cm.output[0],
            'WARNING:root:No resumptiontoken element from url '
            'http://localhost:6003/v0/oai?verb=ListRecords&metadataPrefix=some_prefix. '
            'If target of requests is a Kuha OAI-PMH Repo Handler the response for '
            'ListRecords should always contain a resumptionToken element.')

    def test_invalid_HTTP_response_code(self):
        self.maxDiff = None
        self._set_cli_args('some_prefix')
        self._mock_HTTPClient.return_value.fetch.return_value = mock.Mock(code=500, body=b'')
        with self.assertRaises(list_records.InvalidOAIResponse),\
                self.assertLogs(level='ERROR') as cm_log:
            list_records.main()
        self.assertEqual(len(cm_log.output), 1)
        first_line, second_line, last_line = _extract_exception_loglines(cm_log)
        self.assertEqual(first_line,
                         'ERROR:root:Caught InvalidOAIResponse while running the sequence. '
                         'Increase logging level to see response body')
        self.assertEqual(second_line, 'Traceback (most recent call last):')
        self.assertEqual(
            last_line,
            'kuha_oai_pmh_repo_handler.list_records.InvalidOAIResponse: '
            'Got invalid HTTP response code (500 != 200) from url '
            'http://localhost:6003/v0/oai?verb=ListRecords&metadataPrefix=some_prefix')

    def test_invalid_XML_response_body(self):
        self._set_cli_args('some_prefix')
        self._mock_HTTPClient.return_value.fetch.return_value = mock.Mock(code=200, body=b'asd')
        with self.assertRaises(list_records.InvalidOAIResponse),\
                self.assertLogs(level='ERROR') as cm_log:
            list_records.main()
        self.assertEqual(len(cm_log.output), 1)
        first_line, second_line, last_line = _extract_exception_loglines(cm_log)
        self.assertEqual(first_line,
                         'ERROR:root:Caught InvalidOAIResponse while running the sequence. '
                         'Increase logging level to see response body')
        self.assertEqual(second_line, 'Traceback (most recent call last):')
        self.assertEqual(
            last_line,
            'kuha_oai_pmh_repo_handler.list_records.InvalidOAIResponse: '
            'Unable to parse response body as XML. url: '
            'http://localhost:6003/v0/oai?verb=ListRecords&metadataPrefix=some_prefix')

    def test_oaierror_response_body(self):
        self._set_cli_args('some_prefix')
        self._mock_HTTPClient.return_value.fetch.return_value = mock.Mock(
            code=200, body=_oaierror_response_body())
        with self.assertRaises(list_records.InvalidOAIResponse),\
                self.assertLogs(level='ERROR') as cm_log:
            list_records.main()
        self.assertEqual(len(cm_log.output), 1)
        first_line, second_line, last_line = _extract_exception_loglines(cm_log)
        self.assertEqual(first_line,
                         'ERROR:root:Caught InvalidOAIResponse while running the sequence. '
                         'Increase logging level to see response body')
        self.assertEqual(second_line, 'Traceback (most recent call last):')
        self.assertEqual(
            last_line,
            'kuha_oai_pmh_repo_handler.list_records.InvalidOAIResponse: '
            'Error code cannotDisseminateFormat (Metadata format not supported by this '
            'repository) in OAI response from url: '
            'http://localhost:6003/v0/oai?verb=ListRecords&metadataPrefix=some_prefix')


class TestCliSetup(testcases.KuhaUnitTestCase):

    maxDiff = None

    @mock.patch.object(list_records, 'HTTPClient')
    @mock.patch.object(list_records, 'logging')
    @mock.patch('argparse.ArgumentParser')
    def test_setup(self, mock_ArgumentParser, mock_logging, mock_HTTPClient):
        mock_ArgumentParser.return_value.parse_args.return_value = _get_cli_args_mock('prefix')
        mock_HTTPClient.return_value.fetch.return_value = mock.Mock(body=b'<root/>', code=200)
        # CALL
        list_records.main()
        # ASSERT
        mock_ArgumentParser.assert_called_once_with(
            description='Run ListRecords sequence on-demand against an OAI-PMH repo handler '
            'to test that all records are harvestable. '
            'If any error conditions are encountered, the best '
            'place to look for the cause is the Kuha OAI-PMH Repo Handler log output and '
            'Kuha Document Store log output. By default the script outputs every identifier '
            'it encounters to stdout. See --help for more options.')
        self.assertEqual(mock_ArgumentParser.return_value.add_argument.call_count, 8)
        mock_ArgumentParser.return_value.add_argument.assert_has_calls([
            mock.call('metadata_prefix', type=str, help='MetadataPrefix used in requests'),
            mock.call(
                '-b', '--base-url', default='http://localhost:6003/v0/oai',
                help='Base url to oai repo handler (default: http://localhost:6003/v0/oai)',
                required=False, type=str),
            mock.call(
                '-t', '--request-timeout', default=30,  required=False,
                help='HTTP client request timeout. (default: 30)', type=int),
            mock.call(
                '-s', '--set', dest='oai_set', required=False, type=str,
                help='Harvest selectively using a set parameter'),
            mock.call(
                '-f', '--from', dest='oai_from', required=False, type=str,
                help='Harvest selectively using from parameter'),
            mock.call(
                '-u', '--until', dest='oai_until', required=False, type=str,
                help='Harvest selectively using until parameter'),
            mock.call(
                '-o', '--output', required=False, type=str,
                help='Output found identifiers to file instead of stdout.'),
            mock.call(
                '--loglevel', choices=['DEBUG', 'INFO', 'WARNING', 'ERROR'],
                default='INFO', type=str,
                help='Loglevel (default: INFO). Set to DEBUG to log out every response body')
        ])
        mock_ArgumentParser.return_value.parse_args.assert_called_once_with()
        mock_logging.basicConfig.assert_called_once_with(
            datefmt='%Y-%m-%dT%H:%M:%SZ',
            format='%(asctime)s %(levelname)s : %(message)s',
            level=mock_logging.INFO)
        mock_HTTPClient.assert_called_once_with()
        mock_HTTPClient.return_value.fetch.assert_called_once_with(
            'http://localhost:6003/v0/oai?verb=ListRecords&metadataPrefix=prefix',
            request_timeout=30)

    @mock.patch.object(list_records._Request, 'run_sequence',
                       side_effect=TypeError)
    @mock.patch('argparse.ArgumentParser')
    def test_logs_and_reraises_exceptions(self, mock_ArgumentParser,
                                          mock_run):
        mock_ArgumentParser.return_value.parse_args.return_value = mock.Mock(
            loglevel='INFO')
        with self.assertRaises(TypeError), self.assertLogs(level='ERROR') as cm_log:
            list_records.main()
        self.assertEqual(len(cm_log.output), 1)
        first_line, second_line, last_line = _extract_exception_loglines(cm_log)
        self.assertEqual(first_line, 'ERROR:root:Caught critical error. Exiting...')
        self.assertEqual(second_line, 'Traceback (most recent call last):')
        self.assertEqual(last_line, 'TypeError')

    @mock.patch.object(list_records._Request, 'run_sequence',
                       side_effect=KeyboardInterrupt)
    @mock.patch('argparse.ArgumentParser')
    def test_warns_and_swallows_KeyboardInterrupt(self, mock_ArgumentParser,
                                                  mock_run):
        mock_ArgumentParser.return_value.parse_args.return_value = mock.Mock(
            loglevel='INFO')
        with self.assertLogs(level='WARNING') as cm_log:
            list_records.main()
        self.assertEqual(len(cm_log.output), 1)
        self.assertEqual(cm_log.output[0], 'WARNING:root:Interrupt by CTRL-C')
