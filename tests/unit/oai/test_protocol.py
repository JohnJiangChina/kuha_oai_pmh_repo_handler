#!/usr/bin/env python3
# Author(s): Toni Sissala
# Copyright 2020 Finnish Social Science Data Archive FSD / University of Tampere
# Licensed under the EUPL. See LICENSE.txt for full license.

import datetime
from unittest.mock import Mock
import pytest
from kuha_oai_pmh_repo_handler.oai import protocol


VAL_SET = 'some:set'
VAL_FROM = '2000-01-01'
VAL_UNTIL = '2000-12-31'
VAL_METADATA_PREFIX = 'pre_fix'


# as_supported_datetime

@pytest.mark.parametrize('datetime_str', [
    'asd',
    '2000-13-13',
    '2000-01',
    '2000-01-0109:35:50Z',
    '2000-01-01A09:35:50Z',
    '2000-01-01T09:35:50T'
])
def test_as_supported_datetime_raises_BadArgument_on_invalid_datetime_str(datetime_str):
    with pytest.raises(protocol.oaierrors.BadArgument):
        protocol.as_supported_datetime(datetime_str)


@pytest.mark.parametrize('datetime_str, expected_exc', [
    ('asd', KeyError),
    ('2000-13-13', ValueError),
    ('2000-01', KeyError),
    ('2000-01-0109:35:50Z', KeyError),
    ('2000-01-01A09:35:50Z', ValueError),
    ('2000-01-01T09:35:50T', ValueError)
])
def test_as_supported_datetime_does_not_mask_if_explicitly_commanded(datetime_str,
                                                                     expected_exc):
    # Using raise_oai_exc = False will not mask exceptions.
    with pytest.raises(expected_exc):
        protocol.as_supported_datetime(datetime_str, raise_oai_exc=False)


@pytest.mark.parametrize('datetime_str, expected', [
    ('2000-12-31', datetime.datetime(2000, 12, 31)),
    ('1999-01-01T14:31:59Z', datetime.datetime(1999, 1, 1, 14, 31, 59))
])
def test_as_supported_datetime_success(datetime_str, expected):
    assert protocol.as_supported_datetime(datetime_str) == expected


# ResumptionToken


def test_resumptiontoken_get_metadata_prefix_success():
    token = protocol.ResumptionToken(metadata_prefix=VAL_METADATA_PREFIX)
    assert token.metadata_prefix.value == VAL_METADATA_PREFIX


# OAIArguments


@pytest.mark.parametrize('oaiarguments, expected', [
    ({'set_': VAL_SET, 'from_': VAL_FROM, 'until': VAL_UNTIL}, True),
    ({'set_': VAL_SET, 'from_': VAL_FROM}, True),
    ({'set_': VAL_SET}, True),
    ({'from_': VAL_FROM}, True),
    ({'until': VAL_UNTIL}, True),
    ({'from_': VAL_FROM, 'until': VAL_UNTIL}, True),
    ({'set_': VAL_SET, 'until': VAL_UNTIL}, True),
    ({}, False)
])
def test_oaiarguments_is_selective_success(oaiarguments, expected):
    arg = protocol.OAIArguments('ListIdentifiers', metadata_prefix='oai_dc', **oaiarguments)
    assert arg.is_selective() == expected


@pytest.mark.parametrize('args', [
    [Mock(spec=['set_', 'from_', 'until'], set_=VAL_SET, from_=VAL_FROM, until=VAL_UNTIL), 2],
    [],
])
def test_oaiarguments_is_selective_raises_typeerror(args):
    with pytest.raises(TypeError):
        protocol.OAIArguments.is_selective(*args)


# OAIResponse

def test_OAIResponse_init_with_request_url():
    val = 'some.request.url'
    response_object = protocol.OAIResponse(val)
    response_dict = response_object.get_response()
    assert response_dict['response_metadata']['request_url'] == val


def test_OAIResponse_init_without_request_url():
    response_object = protocol.OAIResponse()
    response_dict = response_object.get_response()
    assert response_dict['response_metadata']['request_url'] == response_object.base_url


def test_OAIResponse_set_base_url_sets_base_url():
    val = 'new_base_url'
    assert protocol.OAIResponse.base_url != val
    protocol.OAIResponse.set_base_url(val)
    assert protocol.OAIResponse.base_url == val
