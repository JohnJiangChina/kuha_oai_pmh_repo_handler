#!/usr/bin/env python3
# Author(s): Toni Sissala
# Copyright 2020 Finnish Social Science Data Archive FSD / University of Tampere
# Licensed under the EUPL. See LICENSE.txt for full license.
"""End-to-end tests against OAI-PMH Repo Handler endpoints.

.. Note::  Environment variables KUHA_DS_URL & KUHA_OAI_URL must be set.
"""
import os
import random
import string
import unittest
import xml.etree.ElementTree as ET
from kuha_common.testing import time_me
from kuha_common.testing.testcases import KuhaEndToEndTestCase

from kuha_common.document_store.records import Study

REQ_TIMEOUT_DDIC_RECORD = 30
NAMESPACES = {'oai': 'http://www.openarchives.org/OAI/2.0/',
              'xml': 'http://www.w3.org/XML/1998/namespace',
              'ddi25': 'ddi:codebook:2_5'}
MD_PREFIX_DDI_C = 'ddi_c'
MD_PREFIX_OAI_DC = 'oai_dc'
MD_PREFIX_OAI_DDI25 = 'oai_ddi25'
MD_PREFIX_EAD3 = 'ead3'
MD_PREFIXES = [
    MD_PREFIX_DDI_C,
    MD_PREFIX_OAI_DC,
    MD_PREFIX_OAI_DDI25,
    MD_PREFIX_EAD3
]


def get_ddi25anlyunit_from_response_body(response_body):
    root = ET.fromstring(response_body)
    return root.find(('oai:GetRecord/oai:record/oai:metadata/ddi25:codeBook/'
                      'ddi25:stdyDscr/ddi25:stdyInfo/ddi25:sumDscr/ddi25:anlyUnit'), NAMESPACES)


def get_ddi25timemeth_from_response_body(response_body):
    root = ET.fromstring(response_body)
    return root.find(('oai:GetRecord/oai:record/oai:metadata/ddi25:codeBook/'
                      'ddi25:stdyDscr/ddi25:method/ddi25:dataColl/ddi25:timeMeth'), NAMESPACES)


def get_ddi25sampproc_from_response_body(response_body):
    root = ET.fromstring(response_body)
    return root.find(('oai:GetRecord/oai:record/oai:metadata/ddi25:codeBook/'
                      'ddi25:stdyDscr/ddi25:method/ddi25:dataColl/ddi25:sampProc'), NAMESPACES)


def get_ddi25collmode_from_response_body(response_body):
    root = ET.fromstring(response_body)
    return root.find(('oai:GetRecord/oai:record/oai:metadata/ddi25:codeBook/'
                      'ddi25:stdyDscr/ddi25:method/ddi25:dataColl/ddi25:collMode'), NAMESPACES)


def get_ddi25keyword_from_response_body(response_body):
    root = ET.fromstring(response_body)
    return root.find(('oai:GetRecord/oai:record/oai:metadata/ddi25:codeBook/'
                      'ddi25:stdyDscr/ddi25:stdyInfo/ddi25:subject/ddi25:keyword'), NAMESPACES)


def get_ddi25topclas_from_response_body(response_body):
    root = ET.fromstring(response_body)
    return root.find(('oai:GetRecord/oai:record/oai:metadata/ddi25:codeBook/'
                      'ddi25:stdyDscr/ddi25:stdyInfo/ddi25:subject/ddi25:topcClas'), NAMESPACES)


def get_xml_lang_from_element(element):
    return element.attrib.get('{%s}lang' % (NAMESPACES.get('xml'),))


class TestOAIPMHRepoHandlerResponses(KuhaEndToEndTestCase):
    """Make HTTP requests against OAI-PMH Repo Handler endpoints.

    General tests against large collection of records.

    * Setup dummydata to test against.
    * Print endpoint response times.

    .. warning:: Will delete all records from Document Store.
    """

    nof_studies = 200
    nof_vars_per_study = 50
    nof_questions_per_var = 2
    oai_url = ''
    oai_ns_id = None
    posted_study_numbers = []

    @classmethod
    def setUpClass(cls):
        cls.load_env()
        super(TestOAIPMHRepoHandlerResponses, cls).setUpClass()
        cls.DELETE_to_document_store()
        for study_index in range(cls.nof_studies):
            study = cls.generate_dummy_study()
            cls.POST_to_document_store(study)
            cls.posted_study_numbers.append(study.study_number.get_value())
            for variable_index in range(cls.nof_vars_per_study):
                var = cls.generate_dummy_variable()
                var.add_study_number(study.study_number.get_value())
                cls.POST_to_document_store(var)
                for question_index in range(cls.nof_questions_per_var):
                    question = cls.generate_dummy_question()
                    question.add_study_number(study.study_number.get_value())
                    question.add_variable_name(var.variable_name.get_value())
                    cls.POST_to_document_store(question)

    @classmethod
    def load_env(cls):
        try:
            url = os.environ['KUHA_OAI_URL']
        except KeyError:
            raise unittest.SkipTest("KUHA_OAI_URL environment variable is not set")
        cls.oai_url = '/'.join([url, 'oai'])
        try:
            cls.oai_ns_id = os.environ['KUHA_OPRH_OP_NAMESPACE_ID']
        except KeyError:
            pass

    @classmethod
    def get_oai_record_url(cls, metadataprefix, identifier):
        if cls.oai_ns_id:
            return '{}?verb=GetRecord&metadataPrefix={}&identifier=oai:{}:{}'\
                .format(cls.oai_url, metadataprefix, cls.oai_ns_id, identifier)
        return '{}?verb=GetRecord&metadataPrefix={}&identifier={}'.format(cls.oai_url, metadataprefix, identifier)

    @property
    def random_study_identifier(self):
        identifier = random.choice(self.posted_study_numbers)
        if self.oai_ns_id:
            identifier = 'oai:{}:{}'.format(self.oai_ns_id, identifier)
        return identifier

    def _assert_response_body_has_no_errors(self, body):
        root = ET.fromstring(body)
        error_element = root.find('oai:error', NAMESPACES)
        error_text = error_element.text if error_element is not None else ''
        self.assertEqual(error_element, None, msg="Error in OAI response body: {}".format(error_text))

    # Simple tests against endpoints.

    @time_me
    def test_identify_GET(self):
        """Make sure GET against Identify works"""
        response = self.http_client.fetch(self.oai_url + '?verb=Identify')
        self.assertEqual(response.code, 200)
        self._assert_response_body_has_no_errors(response.body)

    @time_me
    def test_identify_POST(self):
        """Make sure POST against Identify works"""
        response = self.http_client.fetch(self.oai_url, method='POST',
                                          headers=self.POST_FORM_HEADERS,
                                          body='verb=Identify')
        self.assertEqual(response.code, 200)
        self._assert_response_body_has_no_errors(response.body)

    def _check_list_sets_body(self, body):
        root = ET.fromstring(body)
        sets = root.findall('oai:ListSets/oai:set', NAMESPACES)
        self.assertGreaterEqual(len(sets), 3)

    @time_me
    def test_list_sets_GET(self):
        response = self.http_client.fetch(self.oai_url + '?verb=ListSets')
        self.assertEqual(response.code, 200)
        self._assert_response_body_has_no_errors(response.body)
        self._check_list_sets_body(response.body)

    @time_me
    def test_list_sets_POST(self):
        response = self.http_client.fetch(self.oai_url, method='POST',
                                          headers=self.POST_FORM_HEADERS,
                                          body='verb=ListSets')
        self.assertEqual(response.code, 200)
        self._assert_response_body_has_no_errors(response.body)
        self._check_list_sets_body(response.body)

    @time_me
    def test_list_metadata_formats_GET(self):
        response = self.http_client.fetch(self.oai_url + '?verb=ListMetadataFormats')
        self.assertEqual(response.code, 200)
        self._assert_response_body_has_no_errors(response.body)

    @time_me
    def test_list_metadata_formats_POST(self):
        response = self.http_client.fetch(self.oai_url, method='POST',
                                          headers=self.POST_FORM_HEADERS,
                                          body='verb=ListMetadataFormats')
        self.assertEqual(response.code, 200)
        self._assert_response_body_has_no_errors(response.body)

    @time_me
    def test_list_identifiers_GET_ddic(self):
        url = self.oai_url + '?verb=ListIdentifiers&metadataPrefix={}'.format(MD_PREFIX_DDI_C)
        response = self.http_client.fetch(url)
        self.assertEqual(response.code, 200)
        self._assert_response_body_has_no_errors(response.body)

    @time_me
    def test_list_identifiers_GET_oai_ddi25(self):
        url = self.oai_url + '?verb=ListIdentifiers&metadataPrefix={}'.format(MD_PREFIX_OAI_DDI25)
        response = self.http_client.fetch(url)
        self.assertEqual(response.code, 200)
        self._assert_response_body_has_no_errors(response.body)

    @time_me
    def test_list_identifiers_GET_oai_dc(self):
        url = self.oai_url + '?verb=ListIdentifiers&metadataPrefix={}'.format(MD_PREFIX_OAI_DC)
        response = self.http_client.fetch(url)
        self.assertEqual(response.code, 200)
        self._assert_response_body_has_no_errors(response.body)

    @time_me
    def test_list_identifiers_POST_ddic(self):
        response = self.http_client.fetch(self.oai_url, method='POST',
                                          headers=self.POST_FORM_HEADERS,
                                          body='verb=ListIdentifiers&metadataPrefix={}'.format(MD_PREFIX_DDI_C))
        self.assertEqual(response.code, 200)
        self._assert_response_body_has_no_errors(response.body)

    @time_me
    def test_list_identifiers_POST_oai_ddi25(self):
        response = self.http_client.fetch(self.oai_url, method='POST',
                                          headers=self.POST_FORM_HEADERS,
                                          body='verb=ListIdentifiers&metadataPrefix={}'.format(MD_PREFIX_OAI_DDI25))
        self.assertEqual(response.code, 200)
        self._assert_response_body_has_no_errors(response.body)

    @time_me
    def test_list_identifiers_POST_oai_dc(self):
        response = self.http_client.fetch(self.oai_url, method='POST',
                                          headers=self.POST_FORM_HEADERS,
                                          body='verb=ListIdentifiers&metadataPrefix={}'.format(MD_PREFIX_OAI_DC))
        self.assertEqual(response.code, 200)
        self._assert_response_body_has_no_errors(response.body)

    @time_me
    def test_list_records_GET_ddic(self):
        url = self.oai_url + '?verb=ListRecords&metadataPrefix={}'.format(MD_PREFIX_DDI_C)
        response = self.http_client.fetch(url, request_timeout=REQ_TIMEOUT_DDIC_RECORD)
        self.assertEqual(response.code, 200)
        self._assert_response_body_has_no_errors(response.body)

    @time_me
    def test_list_records_GET_oai_ddi25(self):
        url = self.oai_url + '?verb=ListRecords&metadataPrefix={}'.format(MD_PREFIX_OAI_DDI25)
        response = self.http_client.fetch(url)
        self.assertEqual(response.code, 200)
        self._assert_response_body_has_no_errors(response.body)

    @time_me
    def test_list_records_GET_oai_dc(self):
        url = self.oai_url + '?verb=ListRecords&metadataPrefix={}'.format(MD_PREFIX_OAI_DC)
        response = self.http_client.fetch(url)
        self.assertEqual(response.code, 200)
        self._assert_response_body_has_no_errors(response.body)

    @time_me
    def test_list_records_POST_ddic(self):
        response = self.http_client.fetch(self.oai_url, method='POST',
                                          headers=self.POST_FORM_HEADERS,
                                          request_timeout=REQ_TIMEOUT_DDIC_RECORD,
                                          body='verb=ListRecords&metadataPrefix={}'.format(MD_PREFIX_DDI_C))
        self.assertEqual(response.code, 200)
        self._assert_response_body_has_no_errors(response.body)

    @time_me
    def test_list_records_POST_oai_ddi25(self):
        response = self.http_client.fetch(self.oai_url, method='POST',
                                          headers=self.POST_FORM_HEADERS,
                                          body='verb=ListRecords&metadataPrefix={}'.format(MD_PREFIX_OAI_DDI25))
        self.assertEqual(response.code, 200)
        self._assert_response_body_has_no_errors(response.body)

    @time_me
    def test_list_records_POST_oai_dc(self):
        response = self.http_client.fetch(self.oai_url, method='POST',
                                          headers=self.POST_FORM_HEADERS,
                                          body='verb=ListRecords&metadataPrefix={}'.format(MD_PREFIX_OAI_DC))
        self.assertEqual(response.code, 200)
        self._assert_response_body_has_no_errors(response.body)

    @time_me
    def test_get_record_GET_ddic(self):
        url = self.oai_url + '?verb=GetRecord&metadataPrefix={}&identifier={}'.format(MD_PREFIX_DDI_C,
                                                                                      self.random_study_identifier)
        response = self.http_client.fetch(url)
        self.assertEqual(response.code, 200)
        self._assert_response_body_has_no_errors(response.body)

    @time_me
    def test_get_record_GET_oai_ddi25(self):
        url = self.oai_url + '?verb=GetRecord&metadataPrefix={}&identifier={}'.format(MD_PREFIX_OAI_DDI25,
                                                                                      self.random_study_identifier)
        response = self.http_client.fetch(url)
        self.assertEqual(response.code, 200)
        self._assert_response_body_has_no_errors(response.body)

    @time_me
    def test_get_record_GET_oai_dc(self):
        url = self.oai_url + '?verb=GetRecord&metadataPrefix={}&identifier={}'.format(MD_PREFIX_OAI_DC,
                                                                                      self.random_study_identifier)
        response = self.http_client.fetch(url)
        self.assertEqual(response.code, 200)
        self._assert_response_body_has_no_errors(response.body)

    @time_me
    def test_get_record_POST_ddic(self):
        response = self.http_client.fetch(self.oai_url, method='POST',
                                          headers=self.POST_FORM_HEADERS,
                                          body='verb=GetRecord&metadataPrefix={}&identifier={}'.format(
                                              MD_PREFIX_DDI_C, self.random_study_identifier))
        self.assertEqual(response.code, 200)
        self._assert_response_body_has_no_errors(response.body)

    @time_me
    def test_get_record_POST_oai_ddi25(self):
        response = self.http_client.fetch(self.oai_url, method='POST',
                                          headers=self.POST_FORM_HEADERS,
                                          body='verb=GetRecord&metadataPrefix={}&identifier={}'.format(
                                              MD_PREFIX_OAI_DDI25, self.random_study_identifier))
        self.assertEqual(response.code, 200)
        self._assert_response_body_has_no_errors(response.body)

    @time_me
    def test_get_record_POST_oai_dc(self):
        response = self.http_client.fetch(self.oai_url, method='POST',
                                          headers=self.POST_FORM_HEADERS,
                                          body='verb=GetRecord&metadataPrefix={}&identifier={}'.format(
                                              MD_PREFIX_OAI_DC, self.random_study_identifier))
        self.assertEqual(response.code, 200)
        self._assert_response_body_has_no_errors(response.body)

    # More complex tests that make assertions from the body.

    def test_list_metadataformats_returns_supported_formats(self):
        """List of metadata formats must contain exactly the ones that we should support.
        """
        response = self.http_client.fetch(self.oai_url + '?verb=ListMetadataFormats')
        self.assertEqual(response.code, 200)
        root = ET.fromstring(response.body)
        found_prefixes = []
        for prefix_element in root.findall('oai:ListMetadataFormats/oai:metadataFormat/oai:metadataPrefix', NAMESPACES):
            found_prefixes.append(prefix_element.text)
        self.assertCountEqual(found_prefixes, MD_PREFIXES)

    def _test_get_record_payload(self, metadata_prefix, identifier):
        url = self.oai_url + '?verb=GetRecord&metadataPrefix={}&identifier={}'.format(metadata_prefix,
                                                                                      identifier)
        response = self.http_client.fetch(url)
        # Assert
        self.assertEqual(response.code, 200)
        root = ET.fromstring(response.body)
        self.assertEqual(root.tag, '{%s}OAI-PMH' % (NAMESPACES.get('oai'),), msg='Invalid root element in response')
        response_identifier = root.find('oai:GetRecord/oai:record/oai:header/oai:identifier', NAMESPACES).text
        self.assertEqual(response_identifier, identifier, msg="Invalid identifier in response")
        self.assertEqual(len(root.findall('oai:GetRecord/oai:record/oai:metadata', NAMESPACES)), 1)
        return response

    @time_me
    def test_get_record_payload_ddic(self):
        """Assert DDI_C record has one and only one metadata-element in GetRecord request.
        """
        self._test_get_record_payload(MD_PREFIX_DDI_C, self.random_study_identifier)

    @time_me
    def test_get_record_payload_cdc_ddi25(self):
        """Assert OAI_DDI25 record has one and only one metadata-element in GetRecord request.
        """
        self._test_get_record_payload(MD_PREFIX_OAI_DDI25, self.random_study_identifier)

    @time_me
    def test_get_record_payload_oai_dc(self):
        """Assert OAI_DC record has one and only one metadata-element in GetRecord request.
        """
        self._test_get_record_payload(MD_PREFIX_OAI_DC, self.random_study_identifier)

    def _test_list_records_list_request_sequence(self, metadata_prefix, request_timeout=20):
        url = self.oai_url + '?verb=ListRecords&metadataPrefix={}'.format(metadata_prefix)
        response = self.http_client.fetch(url, request_timeout=request_timeout)
        self.assertEqual(response.code, 200)
        root = ET.fromstring(response.body)
        while root.find('./oai:ListRecords/oai:resumptionToken', NAMESPACES).text is not None:
            resumption_token = root.find('./oai:ListRecords/oai:resumptionToken', NAMESPACES).text
            url = self.oai_url + '?verb=ListRecords&resumptionToken={}'.format(resumption_token)
            response = self.http_client.fetch(url, request_timeout=request_timeout)
            self.assertEqual(response.code, 200)
            root = ET.fromstring(response.body)

    @time_me
    def test_list_records_list_request_sequence_ddic(self):
        """Run the entire list request sequence for DDI_C ListRecords
        """
        self._test_list_records_list_request_sequence(MD_PREFIX_DDI_C, request_timeout=REQ_TIMEOUT_DDIC_RECORD)

    @time_me
    def test_list_records_list_request_sequence_cdc_ddi25(self):
        """Run the entire list request sequence for (CDC) OAI_DDI25 ListRecords
        """
        self._test_list_records_list_request_sequence(MD_PREFIX_OAI_DDI25)

    @time_me
    def test_list_records_list_request_sequence_oai_dc(self):
        """Run the entire list request sequence for OAI_DC ListRecords
        """
        self._test_list_records_list_request_sequence(MD_PREFIX_OAI_DC)

    def _test_list_identifiers_list_request_sequence(self, metadata_prefix):
        url = self.oai_url + '?verb=ListIdentifiers&metadataPrefix={}'.format(metadata_prefix)
        response = self.http_client.fetch(url)
        self.assertEqual(response.code, 200)
        root = ET.fromstring(response.body)
        while root.find('./oai:ListIdentifiers/oai:resumptionToken', NAMESPACES).text is not None:
            resumption_token = root.find('./oai:ListIdentifiers/oai:resumptionToken', NAMESPACES).text
            url = self.oai_url + '?verb=ListIdentifiers&resumptionToken={}'.format(resumption_token)
            response = self.http_client.fetch(url)
            self.assertEqual(response.code, 200)
            root = ET.fromstring(response.body)

    @time_me
    def test_list_identifiers_list_request_sequence_ddic(self):
        """Run the entire list request sequence for DDI_C ListIdentifiers
        """
        self._test_list_identifiers_list_request_sequence(MD_PREFIX_DDI_C)

    @time_me
    def test_list_identifiers_list_request_sequence_cdc_ddi25(self):
        """Run the entire list request sequence for OAI_DDI25 ListIdentifiers
        """
        self._test_list_identifiers_list_request_sequence(MD_PREFIX_OAI_DDI25)

    @time_me
    def test_list_identifiers_list_request_sequence_oai_dc(self):
        """Run the entire list request sequence for OAI_DC ListIdentifiers
        """
        self._test_list_identifiers_list_request_sequence(MD_PREFIX_OAI_DC)


class TestCornerCases(KuhaEndToEndTestCase):
    """Tests against specially crafted Document Store records."""

    @classmethod
    def setUpClass(cls):
        # TestOAIPMHRepoHandlerResponses holds enviconment
        TestOAIPMHRepoHandlerResponses.load_env()
        super(TestCornerCases, cls).setUpClass()
        cls.DELETE_to_document_store()

    def _test_study_conceptual_element_with_only_description(self, study_setter_func, getter_func,
                                                             md_prefix, *md_prefixes):
        """Make sure Repo Handler serves records with only element PCDATA.

        Post study to Document Store. Get it via OAI-PMH Repo Handler.
        Make assertions to check expected results
        """
        md_prefixes = list(md_prefixes)
        md_prefixes.append(md_prefix)
        _id = self.gen_id()
        description = self.gen_val()
        lang = self.gen_val(lenght=4, unique=False, chars=string.ascii_letters)
        study = Study()
        study.add_study_number(_id)
        study_setter_func(study, None, language=lang, description=description)
        self.POST_to_document_store(study)
        for curr_prefix in md_prefixes:
            url = TestOAIPMHRepoHandlerResponses.get_oai_record_url(curr_prefix, _id)
            response = self.http_client.fetch(url)
            element = getter_func(response.body)
            xml_lang = get_xml_lang_from_element(element)
            self.assertEqual(xml_lang, lang)
            self.assertEqual(element.text, description)

    def _test_study_conceptual_element_without_concept_content(self, study_setter_func, getter_func,
                                                               md_prefix, *md_prefixes):
        """Make sure Repo Handler serves records without elements ID.

        Post study to Document Store. Get it via OAI-PMH Repo Handler.
        Make assertions to check expected results
        """
        md_prefixes = list(md_prefixes)
        md_prefixes.append(md_prefix)
        _id = self.gen_id()
        system_name = self.gen_val()
        uri = self.gen_val()
        description = self.gen_val()
        lang = self.gen_val(lenght=4, unique=False, chars=string.ascii_letters)
        study = Study()
        study.add_study_number(_id)
        study_setter_func(study, None, language=lang, description=description, system_name=system_name, uri=uri)
        self.POST_to_document_store(study)
        for curr_prefix in md_prefixes:
            url = TestOAIPMHRepoHandlerResponses.get_oai_record_url(curr_prefix, _id)
            response = self.http_client.fetch(url)
            element = getter_func(response.body)
            concept_element = element.find('ddi25:concept', NAMESPACES)
            vocaburi = concept_element.attrib.get('vocabURI')
            vocab = concept_element.attrib.get('vocab')
            xml_lang = get_xml_lang_from_element(element)
            self.assertEqual(xml_lang, lang)
            self.assertEqual(element.text, description)
            self.assertEqual(vocaburi, uri)
            self.assertEqual(vocab, system_name)
            self.assertEqual(concept_element.text, None)

    def _test_study_conceptual_element_with_only_concept(self, study_setter_func, getter_func, md_prefix, *md_prefixes):
        md_prefixes = list(md_prefixes)
        md_prefixes.append(md_prefix)
        _id = self.gen_id()
        concept_content = self.gen_val()
        lang = self.gen_val(lenght=4, unique=False, chars=string.ascii_letters)
        study = Study()
        study.add_study_number(_id)
        study_setter_func(study, concept_content, language=lang)
        self.POST_to_document_store(study)
        for curr_prefix in md_prefixes:
            url = TestOAIPMHRepoHandlerResponses.get_oai_record_url(curr_prefix, _id)
            response = self.http_client.fetch(url)
            element = getter_func(response.body)
            concept_element = element.find('ddi25:concept', NAMESPACES)
            xml_lang = get_xml_lang_from_element(element)
            self.assertEqual(xml_lang, lang)
            self.assertEqual(concept_element.text, concept_content)

    def test_study_without_anlyunit_concept(self):
        """Make sure Repo Handler serves records without anlyUnit@ID

        Post study to Document Store. Get it via OAI-PMH Repo Handler.
        Make assertions to check expected results
        """
        self._test_study_conceptual_element_without_concept_content(Study.add_analysis_units,
                                                                    get_ddi25anlyunit_from_response_body,
                                                                    MD_PREFIX_DDI_C, MD_PREFIX_OAI_DDI25)

    def test_study_with_anlyunit_description(self):
        """Make sure Repo Handler serves records with only anlyUnit PCDATA.

        Post study to Document Store. Get it via OAI-PMH Repo Handler.
        Make assertions to check expected results
        """
        self._test_study_conceptual_element_with_only_description(Study.add_analysis_units,
                                                                  get_ddi25anlyunit_from_response_body,
                                                                  MD_PREFIX_DDI_C, MD_PREFIX_OAI_DDI25)

    def test_study_with_anlyunit_concept(self):
        """Make sure Repo Handler serves records with only anlyUnit/concept PCDATA.

        Post study to Document Store. Get it via OAI-PMH Repo Handler.
        Make assertions to check expected results
        """
        self._test_study_conceptual_element_with_only_concept(Study.add_analysis_units,
                                                              get_ddi25anlyunit_from_response_body,
                                                              MD_PREFIX_DDI_C, MD_PREFIX_OAI_DDI25)

    def test_study_without_timemeth_concept(self):
        self._test_study_conceptual_element_without_concept_content(Study.add_time_methods,
                                                                    get_ddi25timemeth_from_response_body,
                                                                    MD_PREFIX_DDI_C, MD_PREFIX_OAI_DDI25)

    def test_study_with_timemeth_description(self):
        self._test_study_conceptual_element_with_only_description(Study.add_time_methods,
                                                                  get_ddi25timemeth_from_response_body,
                                                                  MD_PREFIX_DDI_C, MD_PREFIX_OAI_DDI25)

    def test_study_with_timemeth_concept(self):
        self._test_study_conceptual_element_with_only_concept(Study.add_time_methods,
                                                              get_ddi25timemeth_from_response_body,
                                                              MD_PREFIX_DDI_C, MD_PREFIX_OAI_DDI25)

    def test_study_with_sampproc_description(self):
        self._test_study_conceptual_element_with_only_description(Study.add_sampling_procedures,
                                                                  get_ddi25sampproc_from_response_body,
                                                                  MD_PREFIX_DDI_C, MD_PREFIX_OAI_DDI25)

    def test_study_without_sampproc_concept(self):
        self._test_study_conceptual_element_without_concept_content(Study.add_sampling_procedures,
                                                                    get_ddi25sampproc_from_response_body,
                                                                    MD_PREFIX_DDI_C, MD_PREFIX_OAI_DDI25)

    def test_study_with_sampproc_concept(self):
        self._test_study_conceptual_element_with_only_concept(Study.add_sampling_procedures,
                                                              get_ddi25sampproc_from_response_body,
                                                              MD_PREFIX_DDI_C, MD_PREFIX_OAI_DDI25)

    def test_study_with_collmode_description(self):
        self._test_study_conceptual_element_with_only_description(Study.add_collection_modes,
                                                                  get_ddi25collmode_from_response_body,
                                                                  MD_PREFIX_DDI_C, MD_PREFIX_OAI_DDI25)

    def test_study_without_collmode_concept(self):
        self._test_study_conceptual_element_without_concept_content(Study.add_collection_modes,
                                                                    get_ddi25collmode_from_response_body,
                                                                    MD_PREFIX_DDI_C, MD_PREFIX_OAI_DDI25)

    def test_study_with_collmode_concept(self):
        self._test_study_conceptual_element_with_only_concept(Study.add_collection_modes,
                                                              get_ddi25collmode_from_response_body,
                                                              MD_PREFIX_DDI_C, MD_PREFIX_OAI_DDI25)

    def _test_study_element_with_only_description(self, study_setter_func, getter_func, md_prefix, *md_prefixes):
        md_prefixes = list(md_prefixes)
        md_prefixes.append(md_prefix)
        _id = self.gen_id()
        lang = self.gen_val(lenght=4, unique=False, chars=string.ascii_letters)
        description = self.gen_val()
        study = Study()
        study.add_study_number(_id)
        study_setter_func(study, None, language=lang, description=description)
        self.POST_to_document_store(study)
        for curr_prefix in md_prefixes:
            url = TestOAIPMHRepoHandlerResponses.get_oai_record_url(curr_prefix, _id)
            response = self.http_client.fetch(url)
            element = getter_func(response.body)
            self.assertIsNotNone(element, msg="Unable to find correct element from url {}".format(url))
            xml_lang = get_xml_lang_from_element(element)
            self.assertEqual(xml_lang, lang)
            self.assertEqual(element.text, description)

    def test_study_with_keyword_description(self):
        self._test_study_element_with_only_description(Study.add_keywords,
                                                       get_ddi25keyword_from_response_body,
                                                       MD_PREFIX_DDI_C, MD_PREFIX_OAI_DDI25)

    def test_study_with_classification_description(self):
        self._test_study_element_with_only_description(Study.add_classifications,
                                                       get_ddi25topclas_from_response_body,
                                                       MD_PREFIX_DDI_C, MD_PREFIX_OAI_DDI25)
