#!/usr/bin/env python3
# Author(s): Toni Sissala
# Copyright 2020 Finnish Social Science Data Archive FSD / University of Tampere
# Licensed under the EUPL. See LICENSE.txt for full license.

from kuha_common.testing.testcases import KuhaUnitTestCase
from kuha_common.document_store.records import Question

from kuha_oai_pmh_repo_handler.oai import records


class TestOAIHeaders(KuhaUnitTestCase):

    def setUp(self):
        self.orig_nsid = records.OAIHeaders.namespace_identifier

    def tearDown(self):
        records.OAIHeaders.set_namespace_identifier(self.orig_nsid)

    def test_as_local_id_success(self):
        for ns_id, identifier, expected in [
            (None, '1234', '1234'),
            (None, '_1234', '_1234'),
            (None, '12_34', '12_34'),
            (None, '12?34', '12?34'),
            (None, '?12?34', '?12?34'),
            (None, 'a12?34', 'a12?34'),
            (None, 'A12?34', 'A12?34'),
            (None, 'A', 'A'),
            (None, '0', '0'),
            (None, '-', '-'),
            (None, '-1234', '-1234'),
            (None, '123-asd?ASD_qwe(zxc)', '123-asd?ASD_qwe(zxc)'),
            (None, '1-?_()', '1-?_()'),
            (None, 'study.1', 'study.1'),
            # The following local identifiers are supported by OAI-Identifier schema,
            # but not possible with Kuha Document Store. However the regex is now strictly
            # compatible with OAI-Identifier.
            (None, '-', '-'),
            (None, '-', '-'),
            (None, '.', '.'),
            (None, '!', '!'),
            (None, '~', '~'),
            (None, '*', '*'),
            (None, '\'', '\''),
            (None, '(', '('),
            (None, ')', ')'),
            (None, ';', ';'),
            (None, '/', '/'),
            (None, '?', '?'),
            (None, ':', ':'),
            (None, '@', '@'),
            (None, '&', '&'),
            (None, '=', '='),
            (None, '+', '+'),
            (None, '$', '$'),
            (None, ',', ','),
            (None, '%', '%'),

            ('name.space.id', 'oai:name.space.id:1234', '1234'),
            ('name.s', 'oai:name.s:1234', '1234'),
            ('name-space-id', 'oai:name-space-id:1234', None),
            ('name-space-id', 'oai:name.-space-id:1234', None),
            ('nam.e-space-id', 'oai:nam.e-space-id:1234', '1234'),
            ('name.space.id', ':oai:name.space.id:1234', None),
            ('name.space.id', 'oai::name.space.id:1234', None),
            ('name.space.id', 'oai:name.space.id:-1234', '-1234'),
            ('name.space.id', 'oai:name.space.id:12?34-_()', '12?34-_()'),
            ('name.space.id', '12?34', None),
            ('name.space.id', 'name.space.id:12?34', None)
        ]:
            with self.subTest(ns_id=ns_id, identifier=identifier, expected=expected):
                records.OAIHeaders.set_namespace_identifier(ns_id)
                self.assertEqual(records.OAIHeaders.as_local_id(identifier), expected)


class TestOAIRecord(KuhaUnitTestCase):

    def test_add_question_adds_new_question(self):
        study = self.generate_dummy_study()
        oai_record = records.OAIRecord(study)
        self.assertEqual(oai_record.questions, {})
        question = self.generate_dummy_question()
        oai_record.add_question(question)
        self.assertEqual(oai_record.questions,
                         {question.variable_name.get_value(): [question]})

    def test_add_question_appends_question(self):
        study = self.generate_dummy_study()
        oai_record = records.OAIRecord(study)
        question_1 = self.generate_dummy_question()
        question_2 = self.generate_dummy_question()
        question_2.add_variable_name(question_1.variable_name.get_value())
        oai_record.add_question(question_1)
        oai_record.add_question(question_2)
        self.assertEqual(oai_record.questions,
                         {question_1.variable_name.get_value(): [question_1, question_2]})

    def test_add_question_discards_question_without_variable_name(self):
        study = self.generate_dummy_study()
        oai_record = records.OAIRecord(study)
        question = Question()
        question.add_study_number(self.gen_id())
        question.add_question_identifier(self.gen_id())
        question.add_question_texts(self.gen_val(), self.record_lang_1)
        question.add_research_instruments(self.gen_val(), self.record_lang_1)
        question.add_codelist_references(self.gen_val(), self.record_lang_1)
        oai_record.add_question(question)
        self.assertEqual(oai_record.questions, {})
