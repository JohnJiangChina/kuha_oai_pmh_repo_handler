#!/usr/bin/env python3
# Author(s): Toni Sissala
# Copyright 2020 Finnish Social Science Data Archive FSD / University of Tampere
# Licensed under the EUPL. See LICENSE.txt for full license.

import pytest

from kuha_oai_pmh_repo_handler.oai import protocol

VAL_TOKEN_DDI_C = 'cursor:0&from:None&until:2018-02-01T06:53:14Z&completeListSize:1343&metadataPrefix:ddi_c'
VAL_TOKEN_OAI_DC = 'cursor:0&from:None&until:2018-02-01T06:53:14Z&completeListSize:1343&metadataPrefix:oai_dc'


@pytest.mark.parametrize('args', [
    {'verb': 'Identify'},

    {'verb': 'ListSets'},
    {'verb': 'ListSets', 'resumptionToken': VAL_TOKEN_DDI_C},

    {'verb': 'ListIdentifiers', 'metadataPrefix': 'ddi_c',
     'resumptionToken': VAL_TOKEN_DDI_C},
    {'verb': 'ListIdentifiers', 'metadataPrefix': 'oai_dc'},
    {'verb': 'ListIdentifiers', 'metadataPrefix': 'oai_dc', 'set': 'some:set'},
    {'verb': 'ListIdentifiers', 'metadataPrefix': 'oai_dc', 'from': '2000-01-01'},
    {'verb': 'ListIdentifiers', 'metadataPrefix': 'oai_dc', 'until': '2000-01-01'},
    {'verb': 'ListIdentifiers', 'metadataPrefix': 'oai_dc', 'set': 'some:set',
     'from': '2000-01-01', 'until': '2001-01-01'},

    {'verb': 'ListMetadataFormats'},
    {'verb': 'ListMetadataFormats', 'identifier': '1234'},

    {'verb': 'ListRecords', 'metadataPrefix': 'oai_dc'},
    {'verb': 'ListRecords', 'metadataPrefix': 'oai_dc', 'resumptionToken': VAL_TOKEN_OAI_DC},
    {'verb': 'ListRecords', 'resumptionToken': VAL_TOKEN_OAI_DC},
    {'verb': 'ListRecords', 'metadataPrefix': 'oai_dc', 'from': '2000-01-01'},
    {'verb': 'ListRecords', 'metadataPrefix': 'oai_dc', 'until': '2020-12-31'},
    {'verb': 'ListRecords', 'metadataPrefix': 'oai_dc', 'from': '2000-01-01',
     'until': '2020-12-31'},
    {'verb': 'ListRecords', 'metadataPrefix': 'oai_dc', 'set': 'some:set',
     'from': '2000-01-01', 'until': '2020-12-31'},

    {'verb': 'GetRecord', 'identifier': '1234', 'metadataPrefix': 'oai_dc'},
])
def test_oairequest_from_request_args_success(args):
    args_list = [(k, v) for k, v in args.items()]
    rval = protocol.OAIRequest(args_list)
    assert rval.get_verb() == args['verb']
    if 'identifier' in args:
        assert rval.get_identifier() == args['identifier']
    if 'metadataPrefix' in args:
        assert rval.get_metadata_format() is not None
    if 'set' in args:
        assert rval.has_set() is True
        assert rval.get_set() == args['set']
    if any(key in args for key in ['set', 'from', 'until']):
        assert rval.is_selective() is True


@pytest.mark.parametrize('args, expected_error', [
    ([], protocol.oaierrors.MissingVerb),
    ([('verb', 'asd')], protocol.oaierrors.BadVerb),
    # Missing metadataprefix
    ([('verb', 'ListIdentifiers')], protocol.oaierrors.BadArgument),
    ([('verb', 'ListRecords')], protocol.oaierrors.BadArgument),
    ([('verb', 'GetRecord')], protocol.oaierrors.BadArgument),
    # Verb does not support resumptiopntoken
    ([('verb', 'Identify'), ('resumptionToken', VAL_TOKEN_DDI_C)],
     protocol.oaierrors.BadArgument),
    ([('verb', 'ListMetadataFormats'), ('resumptionToken', VAL_TOKEN_DDI_C)],
     protocol.oaierrors.BadArgument),
    ([('verb', 'GetRecord'), ('resumptionToken', VAL_TOKEN_DDI_C)],
     protocol.oaierrors.BadArgument),
    # metadataPrefix in resumptionToken differs from metadataPrefix-parameter.
    ([('verb', 'ListIdentifiers'),
      ('metadataPrefix', 'ddi_c'),
      ('resumptionToken', VAL_TOKEN_OAI_DC)], protocol.oaierrors.BadArgument),
    # Missing identifier
    ([('verb', 'GetRecord'), ('metadataPrefix', 'ddi_c')], protocol.oaierrors.BadArgument),
    # from argument is later than until argument
    ([('verb', 'ListRecords'),
      ('metadataPrefix', 'ddi_c'),
      ('from', '2001-12-31'),
      ('until', '2001-01-01')], protocol.oaierrors.BadArgument)
])
def test_oairequest_from_request_args_raises_oaierrors(args, expected_error):
    with pytest.raises(expected_error):
        protocol.OAIRequest(args)
