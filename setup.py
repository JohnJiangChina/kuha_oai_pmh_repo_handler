#!/usr/bin/env python3
# Author(s): Toni Sissala
# Copyright 2020 Finnish Social Science Data Archive FSD / University of Tampere
# Licensed under the EUPL. See LICENSE.txt for full license.

from setuptools import setup, find_packages


with open('VERSION', 'r') as file_obj:
    version = file_obj.readline().strip()


setup(
    name='kuha_oai_pmh_repo_handler',
    version=version,
    url='',
    description='OAI-PMH Repo Handler for Kuha2 bundle',
    author='Toni Sissala',
    author_email='toni.sissala@tuni.fi',
    packages=find_packages(),
    include_package_data=True,
    tests_require=[
        'pytest',
        'pytest-mock',
        'tornado',
        'kuha_common>=0.14.0',
        'kuha_oai_pmh_repo_handler',
        'genshi'
    ],
    install_requires=[
        'tornado',
        'kuha_common>=0.14.0',
        'genshi'
    ],
    entry_points={
        'console_scripts': [
            'kuha_oai_serve=kuha_oai_pmh_repo_handler.serve:main',
            'kuha_list_records=kuha_oai_pmh_repo_handler.list_records:main'
        ],
    }
)
