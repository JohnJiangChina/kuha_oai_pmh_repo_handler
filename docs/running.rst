Running the Server
------------------

This guide will use convenience scripts from ``scripts`` subdirectory.
It is assumed that the program was installed by using
``scripts/install_kuha_oai_pmh_repo_handler_virtualenv.sh``.

Run OAI-PMH Repo Handler server::

    ./scripts/run_kuha_oai_pmh_repo_handler.sh --oai-pmh-base-url=<base-url> --oai-pmh-admin-email=<admin-email>

The script will source ``scripts/runtime_env`` and activate the
installed virtualenv. Finally it calls ``kuha_oai_serve``, with given
command line arguments.

Ensuring OAI-PMH serves correct records
---------------------------------------

The program contains a helper script to run through all records from
OAI-PMH Repo Handler using OAI verb ListRecords. The script will print
out all identifiers it encounters and log out the time it took to
complete the full ListRecords sequence. Note that the OAI-PMH Repo
Handler server must be running and accessible in order to get correct
results.

If any error conditions are encountered the best place to determine the
cause is Kuha OAI-PMH Repo Handler server log.

Run through all records using ``oai_dc`` metadataprefix::

    ./scripts/list_records.sh oai_dc

See help for more information and configuration options::

    ./scripts/list_records.sh --help
