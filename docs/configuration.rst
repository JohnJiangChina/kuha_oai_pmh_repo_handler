Configuration
-------------

The application can be configured with a configuration file,
via command line arguments or by environment variables.
If a configuration option is specified in more than one place,
then command line values override environment variables which
override configuration file values which override defaults.

.. Note::

   Configuration options for --oai-pmh-base-url and
   --oai-pmh-admin-email are required.

Some of the configuration options configure the OAI-PMH repository.
Refer to `OAI-PMH`_ protocol description for more information.

The following configuration options are available:

.. program:: kuha_oai_serve

.. option:: -h, --help

   Show help message and exit.

.. option:: --print-configuration

   Print active configuration and exit.

.. option:: --port <port>

   Port for serving OAI-PMH Repo Handler. Defaults to ``6003``
   May also be controlled by setting environment variable: ``KUHA_OPRH_PORT``.

.. option:: --template-folder <folder>

   Absolute path to Genshi templates. Defaults to
   ``<package-installation-dir>/templates``.
   May also be controlled by setting environment variable: ``KUHA_OPRH_TEMPLATES``.

.. option:: --oai-pmh-repo-name <repo_name>

   `OAI-PMH`_ repository name. Defauts to ``Kuha2 oai-pmh repository``.
   May also be controlled by setting environment variable: ``KUHA_OPRH_OP_REPO_NAME``.

.. option:: --oai-pmh-base-url <base_url>

   `OAI-PMH`_ base url. **Required** configuration value.
   May also be controlled by setting environment variable: ``KUHA_OPRH_OP_BASE_URL``.

.. option:: --oai-pmh-namespace-identifier <namespace_id>

   Namespace identifier to use with `OAI-Identifiers`_. Set ``None`` to disable
   use of OAI-Identifiers. Defaults to ``None``.
   May also be controlled by setting environment variable: ``KUHA_OPRH_OP_NAMESPACE_ID``.

.. option:: --oai-pmh-protocol-version <version>

   `OAI-PMH`_ protocol version. Note that currently only 2.0 is supported.
   Defaults to ``2.0``. May also be controlled by setting environment variable:
   ``KUHA_OPRH_OP_PROTO_VERSION``.

.. option:: --oai-pmh-results-per-list <results_per_list>

   Set maximum number of results for each list response. Defaults to ``500``.
   May also be controlled by setting environment variable: ``KUHA_OPRH_OP_LIST_SIZE``.

.. option:: --oai-pmh-admin-email <email>

   `OAI-PMH`_ administrator email address. **Required** configuration value.
   Repeat to give multiple addresses. May also be controlled by setting
   environment variable: ``KUHA_OPRH_OP_EMAIL_ADMIN``.

.. option:: --oai-pmh-api-version <api_version>

   Api version for OAI-PMH Repo Handler. This gets prepended to the URL path.
   Defaults to ``v0``. May also be controlled by setting environment variable:
   ``KUHA_OPRH_API_VERSION``.

.. option:: --document-store-host <host>

   Host & scheme of Kuha Document Store. Defaults to ``http://localhost``.
   May also be controlled by setting environment variable: ``KUHA_DS_HOST``.

.. option:: --document-store-port <port>

   Port of Kuha document store database. Defaults to ``6001``.
   May also be controlled by setting environment variable: ``KUHA_DS_PORT``.

.. option:: --document-store-api-version <api_version>

   Api version for document store. This gets prepended to the URL path.
   Defaults to ``v0``. May also be controlled by setting environment
   variable: ``KUHA_DS_API_VERSION``.

.. option:: --document-store-client-request-timeout <timeout>

   Request timeout (in seconds) for Document Store client.
   Defaults to ``120``. May also be controlled by setting environment
   variable: ``KUHA_DOCUMENT_STORE_CLIENT_REQUEST_TIMEOUT``.

.. option:: --document-store-client-connect-timeout <timout>

   Connect timeout (in seconds) for Document Store client.
   Defaults to ``120``. May also be controlled by setting environment
   variable: ``KUHA_DOCUMENT_STORE_CLIENT_CONNECT_TIMEOUT``.

.. option:: --document-store-client-max-clients <max_clients>

   Maximum number of simultaneous client connections for Document Store client.
   Defaults to ``10``. May also be controlled by setting environment
   variable: ``KUHA_DOCUMENT_STORE_CLIENT_MAX_CLIENTS``.

.. option:: --loglevel <loglevel>

   Lowest logging level of log messages that get output. Valid values are
   logging levels supported by Python's :mod:`logging` ``[CRITICAL,ERROR,WARNING,INFO,DEBUG]``.
   Defaults to ``INFO``.
   May also be controlled by setting environment variable: ``KUHA_LOGLEVEL``

.. option:: --logformat <logformat>

   Logging format supported by :mod:`logging`. Defaults to ``%(asctime)s %(levelname)s(%(name)s): %(message)s)``
   May also be controlled by setting environment variable: ``KUHA_LOGFORMAT``
   
**Configuration file**

Args that start with '--' (eg. --document-store-port) can also be set
in a config file. The configuration file lookup searches the file
from current working directory and from the package directory.
The name of the configuration file is ``kuha_oai_pmh_repo_handler.ini``.


.. note:: Invoke with ``--help`` to print out config file lookup paths.

**Environment variables**

If the program will be run by using the scripts provided in ``scripts``
subdirectory, the runtime environment can be controlled via ``scripts/runtime_env``,
which will be created by copying from ``scripts/runtime_env.dist`` at
installation time by ``scripts/install_kuha_oai_pmh_repo_handler_virtualenv.sh``.


.. _oai-pmh: http://www.openarchives.org/OAI/openarchivesprotocol.html
.. _oai-identifiers: http://www.openarchives.org/OAI/2.0/guidelines-oai-identifier.htm
