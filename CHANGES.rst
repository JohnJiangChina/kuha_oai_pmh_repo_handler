Kuha OAI-PMH Repo Handler Changelog
===================================


0.13.0 (2021-02-02)
-------------------

* Changes to EAD3 mapping:

  * /ead/archdesc/dsc/c01/c02/did/daoset/dao/@daotype value is now
    "unknown" instead of "derived"
  * study.principal_investigators are now divided into corpname &
    persname elements. If a principal investigator has an affiliated
    organization the value is placed into persname and the
    organization is placed into corpname. If a principal investigator
    has no affiliated organization, it's value is expected to be an
    organization and the value is placed into corpname.

* Lock pip version to 20.3.4 in install script, which is the latest
  pip that supports Python 3.5. The install script should be
  compatible with Ubuntu 16.04, which defaults to Python 3.5. The
  latest pip does not support Python 3.5 and therefore cannot be
  upgraded into.
* Use ''python-latest'' in Jenkinsfile. It is guaranteed to point to
  the latest python on the CI server. Note that this is a CI specific
  configuration and is not portable to some arbitrary Jenkins
  instance.
* Add py39 to tox environments and use it in Jenkinsfile.
* Upgrade to latest genshi 0.7.5 in requirements.txt. Python 3.9
  introduced changes to ast module that were incompatible with
  previous genshi version 0.7.3 used.
* Add six to requirements.txt, since genshi 0.7.5 requires it.


0.12.1 (2020-12-08)
-------------------

* Fix unhandled AttributeError, when requesting a list response with
  an unsupported set-parameter. Respond with OAI error code
  "noRecordsMatch". (Fixes `#19`_)


0.12.0 (2020-06-12)
-------------------

* Support Python 3.8: involves code changes to tests and requirement of
  kuha_common 0.14.0. No new functionality or bug fixes are introduced.


0.11.1 (2020-06-05)
-------------------

* :func:`kuha_oai_pmh_repo_handler.oai.records.get_sets_list_from_query_result`
  Check for query_result item's set.record_field_setspec using dict's get-method,
  since the item may not have such key. Remove check for the record_field_setname,
  since it is acceptable to have an OAI set without a setName. (Fixes `#18`_)


0.11.0 (2020-05-07)
-------------------

* Use Study.related_publications to render relpubl elements in DDI 2.5. templates.
* require kuha_common 0.13.0


0.10.0 (2020-04-29)
-------------------

* Project source code management changed to git. Does not contain code changes to
  OAI-PMH Repo Handler.
* INSTALL.rst now instructs to use Git instead of Mercurial.
* requirements.txt: kuha_common 0.12.0


0.9.0 (2020-03-19)
------------------

* Add list_records.py to run through the entire ListRecords sequence on-demand.
* setup.py: Create console script entry point to run list_records.py.
* Add shell script to run the list_records entry point using runtime_env and
  Python virtualenv.


0.8.0 (2020-01-22)
------------------

* Add rights element to OAI-DC serialization.


0.7.1 (2020-01-10)
------------------

* Fixes for EAD3 serialization:

  * Correct schemaLocation declaration.
  * Add missing daotype attribute which is required for dao-element.
  * Don't render empty langmaterial elements.


0.7.0 (2020-01-09)
------------------

* Support for EAD3 metadata with metadataPrefix ``ead3``
* Use study.data_kinds as an OAI-PMH set.
* Render following DDI 2.5. elements in ddi_c and oai_ddi25 metadata:

  * ``/codeBook/stdyDscr/citation/prodStmt/copyright``
  * ``/codeBook/stdyDscr/dataAccs/useStmt/citReq``
  * ``/codeBook/stdyDscr/dataAccs/useStmt/deposReq``
  * ``/codeBook/stdyDscr/stdyInfo/sumDscr/geogCover``
  * ``/codeBook/stdyDscr/stdyInfo/sumDscr/dataKind``

* Make sure ddi_c and oai_ddi25 templates won't render duplicate ID-attributes
  per document.
* Add ``metadataPrefix`` attribute to OAI-Header's request element. (Fixes `#15`_)
* Add ``set``, ``from`` and ``until`` attributes to OAI-Header's request element.
* Clear OAI-Header's request elements attributes if response results in badVerb or
  badArgument.
* Relax regex validating OAI-Identifier. (Fixes `#16`_)
* Add set info to resumptionToken and use it in subsequent queries to Document Store.
  (Fixes `#17`_)
* Fix unhandled exception when requesting a set with more than one colon.
* Update python package requirements:
  * genshi 0.7.3
  * kuha_common 0.10.0


0.6.0 (2019-03-14)
------------------

* Support for kuha_common 0.9.0
* Update copyright headers to 2019.


0.5.0 (2018-12-18)
------------------

* Require kuha_common 0.8.0. for better support for testing tornado handlers.
* Decouple setup of ``template_folder`` from importing handlers.


0.4.0 (2018-09-13)
------------------

* DDI-C template: Handle the possibility of None values in variable.codelist_codes[n].code.
  (Fixes `#14`_)
* Support using ``base_url`` for OAI-PMH request element. (Implements `#6`_)

  * Add configuration option ``--oai-pmh-respond-with-requested-url`` which defaults to False.
    Using this computes the base url for OAI-PMH request element from the HTTP request.

* Require kuha_common>=0.6.0.


0.3.0 (2018-07-12)
------------------

* Pin requirement to kuha_common version in requirements.txt.
  This way it is easier to use older releases.
* Fix possible TypeError in :func:`kuha_oai_pmh_repo_handler.oai.records.is_valid_setspec`,
  by explicit check for None in :func:`kuha_oai_pmh_repo_handler.oai.records.get_set_specs_from_ds_record`.
  (Fixes `#11`_)
* Conceptual container elements should be presented even if no concept is found.
  (Implements `#10`_)
* Fix possible NameError in :meth:`kuha_oai_pmh_repo_handler.oai.OAIRecord.add_question`.
  (Fixes `#12`_)
* Serve topcClas and keyword DDI 2.5 elements even if no @ID attribute can be found.
  (Implements `#13`_)
* Refactor end-to-end tests: Use kuha_common.testing package.


0.2.5 (2018-03-14)
------------------

* Add DDI 2.5 metadata format for CESSDA Data Catalogue.
  (Resolves `#8`_)
* scripts/install_kuha_oai_pmh_repo_handler_virtualenv.sh:
  Use pip to install & upgrade.


0.2.4 (2018-03-09)
------------------

* ListIdentifiers verb handler should not retrieve relative records.
  (Fixes `#7`_)


0.2.3 (2018-03-07)
------------------

* DDI-C metadata & template:

  * Add support for Study.document_uris
  * Add support for Study.publishers.attr_abbreviation
  * Add missing vocabURI-attribute to topClas element
  * Add missing vocabURI-attribute to keyword element

* OAI-DC metadata & template:

  * Use Study.document_uris for identifier.

* Don't require metadataPrefix when request contains resumptionToken.
  (Fixes `#5`_)
* Add upgrade flag to pip install command.


0.2.2 (2018-01-31)
------------------

* DDI-C metadata & template: add support for:

  * Study.identifiers
  * Study.distributors
  * Study.time_methods
  * Study.sampling_procedures
  * Study.collection_modes
  * Study.analysis_units
  * Study.collection_periods
  * Study.data_access_descriptions

* Add support for Study.identifiers in OAI-DC metadata & template.
* Fix incorrect handling of requested identifier
  when using OAI namespace-identifier. (Fixes `#4`_)
* Separate OAI-PMH error messages for *no known item* and
  *invalid identifier structure*


0.2.1 (2017-11-16)
------------------

* Add ID-attribute to qstn-element in DDI-C template


0.2.0 (2017-11-10)
------------------

* Support for variable level metadata in DDI-C


0.1.1 (2017-10-27)
------------------

* Update documentation


0.1.0 (2017-10-25)
------------------

* Initial release


.. _#4: https://bitbucket.org/tietoarkisto/kuha_oai_pmh_repo_handler/issues/4
.. _#5: https://bitbucket.org/tietoarkisto/kuha_oai_pmh_repo_handler/issues/5
.. _#6: https://bitbucket.org/tietoarkisto/kuha_oai_pmh_repo_handler/issues/6
.. _#7: https://bitbucket.org/tietoarkisto/kuha_oai_pmh_repo_handler/issues/7
.. _#8: https://bitbucket.org/tietoarkisto/kuha_oai_pmh_repo_handler/issues/8
.. _#10: https://bitbucket.org/tietoarkisto/kuha_oai_pmh_repo_handler/issues/10
.. _#11: https://bitbucket.org/tietoarkisto/kuha_oai_pmh_repo_handler/issues/11
.. _#12: https://bitbucket.org/tietoarkisto/kuha_oai_pmh_repo_handler/issues/12
.. _#13: https://bitbucket.org/tietoarkisto/kuha_oai_pmh_repo_handler/issues/13
.. _#14: https://bitbucket.org/tietoarkisto/kuha_oai_pmh_repo_handler/issues/14
.. _#15: https://bitbucket.org/tietoarkisto/kuha_oai_pmh_repo_handler/issues/15
.. _#16: https://bitbucket.org/tietoarkisto/kuha_oai_pmh_repo_handler/issues/16
.. _#17: https://bitbucket.org/tietoarkisto/kuha_oai_pmh_repo_handler/issues/17
.. _#18: https://bitbucket.org/tietoarkisto/kuha_oai_pmh_repo_handler/issues/18
.. _#19: https://bitbucket.org/tietoarkisto/kuha_oai_pmh_repo_handler/issues/19
