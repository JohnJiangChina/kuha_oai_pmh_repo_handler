Kuha OAI-PMH Repo Handler
=========================

Kuha OAI-PMH Repo Handler is a HTTP API written in Python for
serving Kuha Document Store records through OAI-PMH.

Kuha OAI-PMH Repo Handler is a part of Open Source software
bundle Kuha2.

Features
--------

OAI-PMH features:

* Selective harvesting with Sets & Datestamps.
* List request sequence with ResumptionTokens.
* OAI-Identifiers.

Supported metadata standards:

* DDI-C 2.5
* EAD3
* OAI-DC

Dependencies & requirements
---------------------------

* Python 3.5 or newer
* **Recommended**: python3-venv 3.5.1 or newer

The software is continuously tested against Python versions 3.5, 3.6,
3.7, 3.8. and 3.9.

**Python packages**

The following can be obtained from Python package index.

* tornado (License: Apache License 2.0)
* Genshi (License: BSD)

Kuha Common is a library used with Kuha2 software. It can be obtained
from https://bitbucket.org/tietoarkisto/kuha_common

* kuha_common (License: EUPL)

License
-------

Kuha OAI-PMH Repo Handler is available under the EUPL. See LICENSE.txt for the full license.
